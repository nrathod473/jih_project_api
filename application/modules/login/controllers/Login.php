<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends MX_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->database();
        $this->load->model('user/mdl_user','mdl_user');
        $this->load->model('department/mdl_department','mdl_department');
        $this->load->model('location/mdl_location','mdl_location');
        $this->load->model('level/mdl_level','mdl_level');
    }
    public function _output($Return=array()){
        /*Set response header*/
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        /*Final JSON response*/
        exit(json_encode($Return));
    }
	public function check_login()
    {
        $Return = array('result'=>'', 'error'=>'','arraydata'=>'');
        if($this->input->post('username') === NULL || $this->input->post('username')==='') {
            $Return['error'] = "Please enter username";
        } else if($this->input->post('username') === NULL || $this->input->post('password')==='') {
            $Return['error'] = "Please enter password";
        }

        if($Return['error']!=''){
            $this->_output($Return);
        }
        
        $username = $this->input->post('username',TRUE);
        $password = sha1($this->input->post('password',TRUE));

        $username_escape = $this->db->escape($username);
        $username_password_check = $this->mdl_user->get_where_custom('username = '.$username_escape.' AND isActive != 0 AND password=',$password)->result();
        if (!empty($username_password_check)) 
        {
            $location = $this->mdl_location->get_where($username_password_check[0]->location_id)->result();
            $department = $this->mdl_department->get_where($username_password_check[0]->department_id)->result();
            $level = $this->mdl_level->get_where($location[0]->level_id)->result();


            $location_parent_name = '';
            $location_parent_of_parent_id = '';
            $location_parent_of_parent_name = '';
            if($location[0]->parent_id != 0)
            {
                $location_parent = $this->mdl_location->get_where($location[0]->parent_id)->result();
                $location_parent_name = $location_parent[0]->location_name;
                if($location_parent[0]->parent_id != 0)
                {
                    $location_parent_of_parent = $this->mdl_location->get_where($location_parent[0]->parent_id)->result();
                    $location_parent_of_parent_id = $location_parent[0]->parent_id;
                    $location_parent_of_parent_name = $location_parent_of_parent[0]->location_name;
                }

            }

            $arraydata = array(
                'user_logged' => TRUE,
                'username' => $username_password_check[0]->username,
                'first_name' => $username_password_check[0]->first_name,
                'last_name' => $username_password_check[0]->last_name,
                'user_id' => $username_password_check[0]->user_id,
                'mobile' => $username_password_check[0]->mobile,
                'department_id' => $username_password_check[0]->department_id,
                'department_name' => $department[0]->department_name,
                'level_id' => $level[0]->id,
                'level_name' => $level[0]->level_name,
                'location_id' => $username_password_check[0]->location_id,
                'location_name' => $location[0]->location_name,
                'location_parent_id' => $location[0]->parent_id,
                'location_parent_name' => $location_parent_name,
                'location_parent_of_parent_id' => $location_parent_of_parent_id,
                'location_parent_of_parent_name' => $location_parent_of_parent_name,

            );
          
            if ($permissions != "") 
            {
                //insert data in auth_table
                //check user id exists in table or not
                $check_active_user = $this->db->where('user_id',$username_password_check[0]->user_id)->get('auth_master')->result();
                $check_active_user_session = 0;
                if (!empty($check_active_user)) 
                {
                    if ($check_active_user[0]->auth_key != '') 
                    {
                        $check_active_user_session = $this->db->like('data',$check_active_user[0]->auth_key)->get('ci_sessions')->num_rows();
                        $arraydata['auth_key'] = $check_active_user[0]->auth_key;
                        //insert in auth_table
                        
                        

                        $auth_data = array(
                            'updated_by' => $arraydata['user_id'],
                            'device_id' => $check_active_user_session>0?$check_active_user_session+1:1,
                            'updated_date' => date('Y-m-d H:i:s'),
                        );
                        $this->db->where('user_id', $arraydata['user_id']);
                        if ($this->db->update('auth_master',$auth_data)) 
                        {
                            //pass
                        }
                        else
                        {
                            $Return['error'] = "Something Went Wrong!";
                        }
                    }
                    else
                    {
                        $salt = $this->generateSalt();
                        if ($salt === FALSE) 
                        {
                            $Return['error'] = "Something Went Wrong!";
                        }
                        else
                        {
                            $hash = crypt($password, $salt);
                            // strlen() is safe since crypt() returns only ascii
                            if (!is_string($hash) || strlen($hash) !== 60) {
                                $Return['error'] = "Something Went Wrong!";
                            }
                            else
                            {
                                $arraydata['auth_key'] = $hash;
                            }
                        }
                        if ($Return['error'] == '') 
                        {
                            //insert in auth_table
                            $auth_data = array(
                                'auth_key' => $arraydata['auth_key'],
                                'updated_by' => $arraydata['user_id'],
                                'device_id' => $check_active_user_session>0?$check_active_user_session+1:1,
                                'updated_date' => date('Y-m-d H:i:s'),
                            );
                            $this->db->where('user_id', $arraydata['user_id']);
                            if ($this->db->update('auth_master',$auth_data)) 
                            {
                                //pass
                            }
                            else
                            {
                                $Return['error'] = "Something Went Wrong!";
                            }
                        }
                        
                    }
                }
                else
                {
                    $salt = $this->generateSalt();
                    if ($salt === FALSE) 
                    {
                        $Return['error'] = "Something Went Wrong!";
                    }
                    else
                    {
                        $hash = crypt($password, $salt);
                        // strlen() is safe since crypt() returns only ascii
                        if (!is_string($hash) || strlen($hash) !== 60) {
                            $Return['error'] = "Something Went Wrong!";
                        }
                        else
                        {
                            $arraydata['auth_key'] = $hash;
                        }
                    }
                    //insert in auth_table
                    $auth_data = array(
                        'auth_key' => $arraydata['auth_key'],
                        'user_id' => $arraydata['user_id'],
                        'device_id' => $check_active_user_session>0?$check_active_user_session+1:1,
                        'created_date' => date('Y-m-d H:i:s'),
                        'updated_date' => date('Y-m-d H:i:s'),
                    );
                    if ($this->db->insert('auth_master',$auth_data)) 
                    {
                        //pass
                    }
                    else
                    {
                        $Return['error'] = "Something Went Wrong!";
                    }
                } 
                if ($Return['error'] == '') 
                {
                    // $this->session->set_userdata($arraydata);

                    $Return['arraydata'] = $arraydata;
                    $Return['result'] = "Welcome ".$username_password_check[0]->username." !";
                }
            }


        }
        else
        {
            $Return['error'] = "Invalid Username or Password!";
        }
        $this->_output($Return);
        exit;
    }
    protected function generateSalt($cost = 13)
    {
        $cost = (int) $cost;
        if ($cost < 4 || $cost > 31) {
            return FALSE;
        }
        else
        {
            // Get a 20-byte random string
            $rand = $this->_generateRandomString(20);
            // Form the prefix that specifies Blowfish (bcrypt) algorithm and cost parameter.
            $salt = sprintf('$2y$%02d$', $cost);
            // Append the random salt data in the required base64 format.
            $salt .= str_replace('+', '.', substr(base64_encode($rand), 0, 22));

            return $salt;
        }
        
    }
    function _generateRandomString($n) { 
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
      
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString; 
    }



    
    
    
}
