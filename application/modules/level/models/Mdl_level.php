<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_level extends CI_Model
{

function __construct() {
parent::__construct();
}

function get_table() {
    $table = "level_master";
    return $table;
}

function get($order_by){
    $table = $this->get_table();
    $this->db->order_by($order_by);
    $query=$this->db->get($table);
    return $query;
}

function get_with_limit($limit, $offset, $order_by, $dir) {
    $table = $this->get_table();
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by,$dir);
    $query=$this->db->get($table);
    return $query;
}

function get_where($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_custom($col, $value) {
    $table = $this->get_table();
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    return $query;
}

function _insert($data){
    $table = $this->get_table();
    
    if ($this->db->insert($table, $data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return FALSE;
    }
}

public function add_level($data)
{
    if ($this->db->insert('level_master',$data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return false;
    }
}

public function update_level($id,$data)
{
    $this->db->query('SELECT id FROM level_master WHERE id = "'.$id.'" FOR UPDATE');
    if ($this->db->where('id',$id)->update('level_master',$data)) 
    {
        return $id;
    }
    else
    {
        return false;
    }
}

public function remove_level($id)
{
    if ($this->db->where('id',$id)->delete('level_master')) 
    {
        return true;
    }
    else
    {
        return false;
    }
}

function _update($id, $data){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->update($table, $data)) 
    {
        return $id;
    }
    else
    {
        return FALSE;
    }
}

function _delete($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function count_where($column, $value) {
    $table = $this->get_table();
    $this->db->where($column, $value);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function count_all() {
    $table = $this->get_table();
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function get_max() {
    $table = $this->get_table();
    $this->db->select_max('id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
}

}