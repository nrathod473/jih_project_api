<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->database();
        $this->load->model('department/mdl_department','mdl_department');
        $this->load->model('location/mdl_location','mdl_location');
    }



    protected function getCharacters($length = 1) {
        $token = '';

        for($index = 1; $index <= $length; $index++) {
            $token = $token . '' . chr(rand(65,90));
        }

        return $token;
    }

   

    protected function generateSalt($cost = 13)
    {
        $cost = (int) $cost;
        if ($cost < 4 || $cost > 31) {
            return FALSE;
        }
        else
        {
            // Get a 20-byte random string
            $rand = $this->_generateRandomString(20);
            // Form the prefix that specifies Blowfish (bcrypt) algorithm and cost parameter.
            $salt = sprintf('$2y$%02d$', $cost);
            // Append the random salt data in the required base64 format.
            $salt .= str_replace('+', '.', substr(base64_encode($rand), 0, 22));

            return $salt;
        }
        
    }
    function _generateRandomString($n) { 
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; 
        $randomString = ''; 
      
        for ($i = 0; $i < $n; $i++) { 
            $index = rand(0, strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        } 
      
        return $randomString; 
    }
    
    public function user_update_post($id=NULL)
    {
        if(authorize("CENTRAL",$this->api_token) && $id != NULL || authorize("STATE",$this->api_token) && $id != NULL) {
            $Return = array();
            $this->db->trans_strict(TRUE);
            $this->db->trans_begin();
            // $id = $this->uri->segment(3);
            
            
            
                $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
                if (empty($user_master)) 
                {
                    $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
                }
                // $company_id = $user_master[0]->company_id;
                //update user
                $current_user = $this->_get_where_custom('user_id',$id)->result();
                if (empty($current_user)) 
                {
                    $response = response("","Invalid User!");
                    $Return[] = 'error';
                    $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
                }
                $data = array(
                    'updated_by' => $this->api_token->user_id,
                    'updated_date' => date('Y-m-d H:i:s'),
                    'version' => $current_user[0]->version+1,
                );
                if ($this->input->post('isActive',TRUE) == '1') 
                {
                    $data['isActive'] = 1;
                }
                else
                {
                    $data['isActive'] = 0;
                }
              

                //update_user
                $update_result =  $this->_update($id,$data);


                if ($update_result != NULL) 
                {
                    $response = response("Data Updated Successfully!","");
                }
                else
                {
                    $response = response("","Something went wrong! Plz try again");
                    $Return[] = 'error';
                }
            
        }
        else{
            $response = response("","You are not an Authorised User!");
            $Return[] = 'error';
        }
        // $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE || !empty($Return))
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
        $this->response($response,MY_Controller::HTTP_OK);
    }

    public function add_user_post()
    {
        if(authorize("CENTRAL",$this->api_token) || authorize("STATE",$this->api_token)){
            $Return = array();
            $this->db->trans_strict(TRUE);
            $this->db->trans_begin();
            if($this->input->post('username')===''){
                $response = response("","Enter User Name");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }else if($this->input->post('contact_number')===''){
                $response = response("","Enter Contact Number");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }else if($this->input->post('password')===''){
                $response = response("","Enter Password");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }else if($this->input->post('password') !== $this->input->post('confirm_password')){
                $response = response("","Confirm password feild doesn't match");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }else if($this->input->post('department')===''){
                $response = response("","Select Department");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }else if($this->input->post('location')===''){
                $response = response("","Select Location");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }

            $username = $this->input->post('username',TRUE);
            $mobile = $this->input->post('contact_number',TRUE);
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $user_check = $this->_get_where_custom('username',$username)->result();
            $mobile_check = $this->_get_where_custom('mobile',$mobile)->result();

            if(!empty($user_check))
            {
                $response = response("","User Name Already Exists!");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }

            if(!empty($mobile_check))
            {
                $response = response("","Contact Number Already Exists!");
                $Return[] = 'error';
                $this->response($response,MY_Controller::HTTP_BAD_REQUEST);
            }
            
            $data = array(
                'first_name' => $this->input->post('first_name',TRUE),
                'last_name' => $this->input->post('last_name',TRUE),
                'email' => $this->input->post('email_id',TRUE),
                'location' => $this->input->post('location',TRUE),
                'department' => $this->input->post('department',TRUE),
                'username' => $this->input->post('username',TRUE),
                'password' => sha1($this->input->post('password')),
                'mobile' => $this->input->post('contact_number',TRUE),
                'isActive' => $this->input->post('contact_number',TRUE),
                'created_by' => $this->api_token->user_id,
                'created_date' => date('Y-m-d H:i:s'),
                'changed_date' => date('Y-m-d H:i:s'),
            );
            if ($this->input->post('isActive',TRUE) == '1') 
            {
                $data['isActive'] = 1;
            }
            
            //add_user
            $result =  $this->_insert($data);
            if ($result != NULL)
            {
                $response = response("Successfully Created User!","");
                $Return[] = 'error';
            } else {
                $response = response("","Something went wrong! Plz try again");
                $Return[] = 'error';
            }
        }
        else {
            $response = response("","You are not an Authorised User!");
            $Return[] = 'error';
        }
        // $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE || !empty($Return))
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
        $this->response($response,MY_Controller::HTTP_OK);
    }


    public function get_user_list_post()
    {
        if(authorize("CENTRAL",$this->api_token) || authorize("STATE",$this->api_token)){
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            // if(authorize("CENTRAL",$this->api_token))
            // {
            //     // of that country state dislpay
            //     $user_list = $this->_get_where_custom('location_id IN (SELECT id FROM location_master WHERE parent_id = "'.$user_master[0]->location_id.'")')->result();
            // }
            // else if(authorize("STATE",$this->api_token))
            // {
            //     // of that state unit_heads dislpay
            //     $user_list = $this->_get_where_custom('location_id IN (SELECT id FROM location_master WHERE parent_id = "'.$user_master[0]->location_id.'")')->result();

            //     $user_list = $this->_get_where_custom('location_id != (SELECT id FROM location_master WHERE parent_id = 0) AND user_id !=',$this->api_token->user_id)->result();
            // }

            $user_list = $this->_get_where_custom('location_id IN (SELECT id FROM location_master WHERE parent_id = "'.$user_master[0]->location_id.'")')->result();

            
            $data = array();
            $i = 1;
            if (!empty($user_list)) 
            {
                foreach($user_list as $r) {
                    $department = $this->mdl_department->get_where($r->department_id)->result();
                    $location = $this->mdl_location->get_where($r->location_id)->result();

                    $data[] = array(
                        'user_id' => $r->user_id,
                        'username' => $r->username,
                        'serial_number' => $i,
                        'full_name' => $r->first_name.' '.$r->last_name,
                        'email' => $r->email,
                        'department_id' => $department[0]->id,
                        'department_name' => $department[0]->department_name,
                        'location_id' => $location[0]->id,
                        'location_name' => $location[0]->location_name,
                        'action' => $r->user_id
                    );
                    $i++;
                    
                }
            }
            
            $this->response($data,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }

    public function get_user_details_post()
    {
        if(authorize("CENTRAL",$this->api_token) || authorize("STATE",$this->api_token)){
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $company_id = $user_master[0]->company_id;
            $is_current_user = $this->input->post('is_current_user',TRUE);
            if($is_current_user!=NULL)
            {
                $user_list = $this->_get_where_custom('company_id',$company_id)->result();

            }
            else
            {
                $user_list = $this->_get_where_custom('user_id != "'.$this->api_token->user_id.'" AND company_id=',$company_id)->result();

            }
            
            $data = array();
            $i = 1;
            if (!empty($user_list)) 
            {
                foreach($user_list as $r) {
                    $data[] = array(
                        'serial_number' => $i,
                        'user_id' => $r->user_id,
                        'username' => $r->username,
                        'full_name' => $r->first_name.' '.$r->last_name,
                        'email' => $r->email,
                    );
                    $i++;
                    
                }
            }
            
            $this->response($data,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }
    
    public function change_password_submit_post()
    {
        $Return = array('result'=>'', 'error'=>'');

        if($this->input->post('old_password')==='') {
            $Return['error'] = "Enter old password";
        } else if($this->input->post('new_password')==='') {
            $Return['error'] = "Enter new password";
        } else if ($this->input->post('new_password')!='' && strlen($this->input->post('new_password')) < 5) {
            $Return['error'] = "Password must have minimum 5 letters!";
        }else if($this->input->post('confirm_new_password')!=$this->input->post('new_password')) {
            $Return['error'] = "Confirm new password and new password not matching!";
        } 

        if($Return['error']!=''){
            $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
        }
        $check_old_password = $this->_get_where($this->api_token->user_id)->result();
        if (empty($check_old_password)) 
        {
            $Return['error'] = "Invalid User!";
        }
        else
        {
            if ($check_old_password[0]->password != sha1($this->input->post('old_password'))) 
            {
                $Return['error'] = "Invalid Old Password!";
            }
        }

        if($Return['error']!=''){
            $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
        }

        $data = array(
            'password' => sha1($this->input->post('new_password')),
            'changed_by' => $this->api_token->user_id,
            'changed_date' => date('Y-m-d H:i:s'),
            'version' => $check_old_password[0]->version+1,
        );

        //add_user
        $result =  $this->_update($this->api_token->user_id,$data);
        if ($result != NULL)
        {
            $Return['result'] = "Password Successfully Updated!";

        }
        else
        {
            $Return['error'] = "Something Went Wrong!";

        }

        $this->response($Return,MY_Controller::HTTP_OK);
        
    }
    public function user_read_post()
    {
        if(authorize("CENTRAL",$this->api_token) || authorize("STATE",$this->api_token)){
            $id = $this->input->post('user_id');
            
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master) || !is_numeric($id)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $result = $this->_get_where_custom('user_id',$id)->result();
            if (!empty($result)) 
            {
                if ($result[0]->created_by != 0) 
                {  
                    $created_by = $this->_get_where($result[0]->created_by)->result()[0]->username;  
                }
                else
                {
                    $created_by = $result[0]->username;
                }
                $data = array();

                $department = $this->mdl_department->get_where($result[0]->department_id)->result();
                $location = $this->mdl_location->get_where($result[0]->location_id)->result();

                
                $data = array(
                    'user_id' => $result[0]->user_id,
                    'first_name' => $result[0]->first_name,
                    'last_name' => $result[0]->last_name,
                    'email' => $result[0]->email,
                    'username' => $result[0]->username,
                    'contact_number' => $result[0]->mobile,
                    'department_id' => $department[0]->id,
                    'department_name' => $department[0]->department_name,
                    'location_id' => $location[0]->id,
                    'location_name' => $location[0]->location_name,
                    'created_by' => $created_by,
                    'created_date' => $result[0]->created_date,
                    'version' => $result[0]->version
                );
                
                $this->response($data,MY_Controller::HTTP_OK);
                // $this->load->view('dialog_user', $data);
            }
            
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }


    
    /* public function delete_user_post()
    {
        if(authorize("D_USER,ROLE_ADMIN",$this->api_token)) {
            $Return = array('result'=>'', 'error'=>'');
            $ids = array();
            $id = $this->input->post('_token',TRUE);
            $user_id = $this->api_token->user_id;
            // $this->load->model('sales_order/mdl_sales_order','mdl_sales_order');
            // $this->load->model('customer/mdl_customer','mdl_customer');
            // $this->load->model('vehicle/mdl_vehicle','mdl_vehicle');
            // $this->load->model('sop/mdl_sop','mdl_sop');
            // $this->load->model('product/mdl_product','mdl_product');
            // $this->load->model('product/mdl_bom_template','mdl_bom_template');
            // $this->load->model('bom/mdl_bom','mdl_bom');
            $this->load->model('branch/mdl_branch_user_allocation','mdl_branch_user_allocation');
           


            // $sales_order_check = $this->mdl_sales_order->get_where_custom('created_by',$id)->result();
            // $customer_check = $this->mdl_customer->get_where_custom('created_by',$id)->result();
            // $vehicle_check = $this->mdl_vehicle->get_where_custom('created_by',$id)->result();
            // $sop_check = $this->mdl_sop->get_where_custom('created_by',$id)->result();
            // $product_check = $this->mdl_product->get_where_custom('created_by',$id)->result();
            // $template_check = $this->mdl_bom_template->get_where_custom('created_by',$id)->result();
            // $bom_check = $this->mdl_bom->get_where_custom('created_by',$id)->result();
            $u_alloc_check = $this->mdl_branch_user_allocation->get_where_custom('user_id',$id)->result();
            // if (!empty($sales_order_check) || !empty($customer_check) || !empty($vehicle_check) || !empty($sop_check) || !empty($product_check) || !empty($template_check) || !empty($bom_check)) 
            // {
            //     $Return['error'] = "This user has many records, you can't delete it.";
            //     $this->output($Return);
            // }

            // if (!empty($u_alloc_check) && empty($sales_order_check) && empty($customer_check) && empty($vehicle_check) && empty($sop_check) && empty($product_check) && empty($template_check) && empty($bom_check)) 
            // {
            if (!empty($u_alloc_check)) 
            {
                $result = $this->mdl_branch_user_allocation->_delete($u_alloc_check[0]->user_allocation_id);
                if($result != NULL) {
                    $result2 = $this->_delete($id);
                    if ($result2 != NULL) {
                        $Return['result'] = "Successfully Deleted Record";
                    }
                    else
                    {
                        $Return['error'] = "Error";

                    }
                    
                } else {
                    $Return['error'] = "Error";
                }
                $this->response($Return,MY_Controller::HTTP_OK);
            }
            else
            {
                $Return['error'] = "User Not Found!";
                $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
            }
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }


    } */





    function _get($order_by)
    {
        $this->load->model('mdl_user');
        $query = $this->mdl_user->get($order_by);
        return $query;
    }

    function _get_with_limit($limit, $offset, $order_by, $dir) 
    {
        if ((!is_numeric($limit)) || (!is_numeric($offset))) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_user');
        $query = $this->mdl_user->get_with_limit($limit, $offset, $order_by, $dir);
        return $query;
    }

    function _get_where($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_user');
        $query = $this->mdl_user->get_where($id);
        return $query;
    }

    function _get_where_custom($col, $value) 
    {
        $this->load->model('mdl_user');
        $query = $this->mdl_user->get_where_custom($col, $value);
        return $query;
    }

    function _insert($data)
    {
        $this->load->model('mdl_user');
        return $this->mdl_user->_insert($data);
    }

    function _update($id, $data)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_user');
        return $this->mdl_user->_update($id, $data);
    }

    function _delete($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_user');
        return $this->mdl_user->_delete($id);
    }

    function _count_where($column, $value) 
    {
        $this->load->model('mdl_user');
        $count = $this->mdl_user->count_where($column, $value);
        return $count;
    }

    function _count_all() 
    {
        $this->load->model('mdl_user');
        $count = $this->mdl_user->count_all();
        return $count;
    }

    function _get_max() 
    {
        $this->load->model('mdl_user');
        $max_id = $this->mdl_user->get_max();
        return $max_id;
    }

    function _custom_query($mysql_query) 
    {
        $this->load->model('mdl_user');
        $query = $this->mdl_user->_custom_query($mysql_query);
        return $query;
    }
    
}
