<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Index extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
    }
    public function index_get()
    {
    	$data = array(
    		"status_code" => MY_Controller::HTTP_OK,
    		"message" => "Success",
    	);
    	$this->response($data,MY_Controller::HTTP_OK);
    }
    public function index_post()
    {
    	$data = array(
    		"status_code" => MY_Controller::HTTP_OK,
    		"message" => "Success",
    	);
    	$this->response($data,MY_Controller::HTTP_OK);
    }
}