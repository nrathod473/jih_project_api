<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Department extends MY_Controller  
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_department','mdl_department');
    }

    public function get_department_post()
	{
		
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
			$data = array();
			$department_list = $this->mdl_department->get_department_list_where()->result();
			$total = $this->mdl_department->count_total();
			$output = array
			(
				//"draw" => $this->input->post('draw'),
				"recordsTotal" => $total,
				"recordsFiltered" => $total,
				"data" => $department_list
			);
			$this->response($output,MY_Controller::HTTP_OK);
		}
	}

    public function edit_department_post()
	{
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
            $id = $_POST['deptid'];
            $data = array();
            $department_list = $this->mdl_department->get_where_custom_ids_in($id)->result();
			$output = array
			(
                "data" => $department_list
            );
            $this->response($output,MY_Controller::HTTP_OK);
		}
	}

    public function add_department_post()
    {
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
            $user_id = $this->api_token->user_id;
            $name = $_POST['department_name'];
            $desc = $_POST['department_description'];
            $data = array(
                'department_name'=>$name,
                'department_description'=>$desc,
                'created_by'=>$user_id,
                'updated_by'=>0,
                'version'=>1,
                'created_date' => date('Y-m-d H:i:s')
            );
            $department = $this->mdl_department->add_dept($data);
		    if($department != NULL) 
			{
				$output['result'] = "Department Succesfully Created!!";
			}
			else
			{
				$output['error'] = "Something went wrong! Plz try again";
			}
            $this->response($output,MY_Controller::HTTP_OK);

        }
    }

    public function update_department_post()
    {
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
            $deptid = $_POST['deptid'];
            $department_list = $this->mdl_department->get_where_custom_ids_in($deptid)->result();
            //echo '<pre>';print_r($department_list[0]->version);exit;
            $user_id = $this->api_token->user_id;
            $name = $_POST['department_name'];
            $desc = $_POST['department_description'];
            $data = array(
                'department_name'=>$name,
                'department_description'=>$desc,
                'updated_by'=>$user_id,
                'version'=>$department_list[0]->version+1,
                'updated_date' => date('Y-m-d H:i:s')
            );
            $department = $this->mdl_department->update_dept($deptid,$data);
		    if($department != NULL) 
			{
				$output['result'] = "Department Updated Succesfully!!";
			}
			else
			{
				$output['error'] = "Something went wrong! Plz try again";
			}
            $this->response($output,MY_Controller::HTTP_OK);

        }
    }

}