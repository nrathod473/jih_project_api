<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class States extends MY_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->database();
    }
    
    public function get_states_post()
    {
        if(authorize("A_CUSTOMER,E_CUSTOMER,LV_CUSTOMER,DV_CUSTOMER,ROLE_ADMIN",$this->api_token)) {
            $data = array();
            $country_id = $this->input->post('country_id');
            $query = $this->_get_where_custom('country_id',$country_id)->result_array();
            if(!empty($query))
            {
                $data = $query;
            }
            $this->response($data,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }
	

    function _get($order_by)
    {
        $this->load->model('mdl_states');
        $query = $this->mdl_states->get($order_by);
        return $query;
    }

    function _get_with_limit($limit, $offset, $order_by, $dir) 
    {
        if ((!is_numeric($limit)) || (!is_numeric($offset))) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_states');
        $query = $this->mdl_states->get_with_limit($limit, $offset, $order_by, $dir);
        return $query;
    }

    function _get_where($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_states');
        $query = $this->mdl_states->get_where($id);
        return $query;
    }

    function _get_where_custom($col, $value) 
    {
        $this->load->model('mdl_states');
        $query = $this->mdl_states->get_where_custom($col, $value);
        return $query;
    }

    function _insert($data)
    {
        $this->load->model('mdl_states');
        return $this->mdl_states->_insert($data);
    }

    function _update($id, $data)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_states');
        return $this->mdl_states->_update($id, $data);
    }

    function _delete($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_states');
        return $this->mdl_states->_delete($id);
    }

    function _count_where($column, $value) 
    {
        $this->load->model('mdl_states');
        $count = $this->mdl_states->count_where($column, $value);
        return $count;
    }

    function _count_all() 
    {
        $this->load->model('mdl_states');
        $count = $this->mdl_states->count_all();
        return $count;
    }

    function _get_max() 
    {
        $this->load->model('mdl_states');
        $max_id = $this->mdl_states->get_max();
        return $max_id;
    }

    function _custom_query($mysql_query) 
    {
        $this->load->model('mdl_states');
        $query = $this->mdl_states->_custom_query($mysql_query);
        return $query;
    }
    
}
