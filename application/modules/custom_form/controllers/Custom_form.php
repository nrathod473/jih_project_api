<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom_form extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('custom_form/mdl_custom_form','mdl_custom_form');
        $this->load->model('custom_form/mdl_custom_form_item','mdl_custom_form_item');
        $this->load->model('enum/mdl_enum','mdl_enum');
        $this->load->model('customer/mdl_customer','mdl_customer');
        $this->load->model('user/mdl_user','mdl_user');
        $this->load->model('custom_form/mdl_custom_form_user_mapper','mdl_custom_form_user_mapper');
        $this->load->model('invoice_reference/mdl_invoice_reference','mdl_invoice_reference');
        
    }


    public function get_custom_form_list_post()
    {
        if(authorize("LV_PROJECT,ROLE_ADMIN",$this->api_token)) {
            $limit = $this->input->post('length');
            $offset = $this->input->post('start');
            $order = 'custom_form_master.custom_form_name';
            $dir = 'asc';//0;

            // $search_field = $this->input->post('search_field',TRUE);
            // $search_value = $this->input->post('search_value',TRUE);
            // $condition = $this->input->post('condition',TRUE);
            $total = 0; 
            $data = array();
            $i = 1;
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $company_id = $user_master[0]->company_id;
            $search = $this->input->post('search[value]');
            if (empty($search))
            {
                $condition = "custom_form_user_mapper_master.user_id = ".$this->api_token->user_id." AND custom_form_master.company_id = '".$company_id."'";
                $total_condition = "custom_form_user_mapper_master.user_id = ".$this->api_token->user_id." AND custom_form_user_mapper_master.company_id = ";
                $total = $this->mdl_custom_form_user_mapper->count_where($total_condition,$company_id);
                $custom_form_list = $this->mdl_custom_form->get_with_limit_where($limit, $offset, $order, $dir,$condition)->result();
                // $custom_form_list = $this->_get_with_limit($limit, $offset, $order, $dir);
                foreach($custom_form_list as $r) {
                    // $this->load->model('product/mdl_product','mdl_product');
                    // $this->load->model('product/mdl_product_model','mdl_product_model');


                    //$product = !empty($this->mdl_product->get_where($r->product_id)->result())?$this->mdl_product->get_where($r->product_id)->result()[0]->description:'';
                    //$product_model_name = !empty($this->mdl_product->get_where($r->product_id)->result())?$this->mdl_product->get_where($r->product_id)->result()[0]->description.' '.$this->mdl_product_model->get_where($r->product_model_id)->result()[0]->model_name:'';

                    $custom_form_date = date_create($r->custom_form_date);
                    $custom_form_date_format = date_format($custom_form_date,"d-M-Y");

                    //$is_purged = FALSE;
                    // if ($product=='') 
                    // {
                    //     $is_purged = TRUE;
                    // }
                    $data[] = array(                    
                        'no' => $i,
                        'custom_form_id' => $r->custom_form_id,
                        'custom_form_name' => $r->custom_form_name,
                        'custom_form_date' => $custom_form_date_format,
                        // 'product' => $product,
                        // 'product_model_name' => $product_model_name,
                        // 'quantity' => $r->quantity,
                        // 'is_purged' => $is_purged,
                        // 'edit_view_delete' => '',
                       );
                       $i++;
                    }
                    
            }
            else
            {
                $total = 0;
                $condition = "custom_form_user_mapper_master.user_id = ".$this->api_token->user_id." AND custom_form_master.custom_form_name LIKE ".$this->db->escape('%'.$search.'%')." AND custom_form_master.company_id = '".$company_id."'";
                $total_condition = "custom_form_user_mapper_master.user_id = ".$this->api_token->user_id." AND custom_form_master.custom_form_name LIKE ".$this->db->escape('%'.$search.'%')." AND custom_form_user_mapper_master.company_id = ";
                $total = $this->mdl_custom_form_user_mapper->count_where($total_condition,$company_id);
                $custom_form_list = $this->mdl_custom_form->get_with_limit_where($limit, $offset, $order, $dir,$condition)->result();
                foreach($custom_form_list as $r) {
                    // $this->load->model('product/mdl_product','mdl_product');
                    // $this->load->model('product/mdl_product_model','mdl_product_model');
                
                    $custom_form_date = date_create($r->custom_form_date);
                    $custom_form_date_format = date_format($custom_form_date,"d-M-Y");
                    
                    $data[] = array(                    
                        'no' => $i,
                        'custom_form_id' => $r->custom_form_id,
                        'custom_form_name' => $r->custom_form_name,
                        'custom_form_date' => $custom_form_date_format,
                        // 'tag' => $r->tag,
                        // 'sales_order' => $sales_order,
                        // 'product' => $product,
                        // 'product_model_name' => $product_model_name,
                        // 'quantity' => $r->quantity,
                        // 'is_purged' => $is_purged,
                        'edit_view_delete' => '',
                       );
                       $i++;
                    }
            }
            $output = array(
                 "recordsTotal" => $total,
                 "recordsFiltered" => $total,
                 "data" => $data
            );

            $this->response($output,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        } 

        
    }

    public function get_custom_form_details_post($id=NULL)
    {
        if(authorize("LV_PROJECT,E_PROJECT,ROLE_ADMIN",$this->api_token)) {
            $Return = array('result'=>'', 'error'=>'');
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $company_id = $user_master[0]->company_id;
            if($id == NULL || !is_numeric($id))
            {
                $Return['error'] = 'Invalid Custom Form!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            else
            {
                $custom_form_master = $this->_get_where_custom('company_id = "'.$company_id.'" AND custom_form_id = ',$id)->result();  
            }
            $custom_form_bucket = array();
            $custom_form_item_bucket = array();
            $custom_form_users_bucket = array();
            if (!empty($custom_form_master)) 
            {
                $r = $custom_form_master[0];
                $custom_form_item_master = $this->mdl_custom_form_item->get_where_custom('custom_form_id',$r->custom_form_id)->result();
                $custom_form_user_mapper_master = $this->mdl_custom_form_user_mapper->get_where_custom('custom_form_id',$r->custom_form_id)->result();
                if (!empty($custom_form_item_master)) 
                {
                    $i = 1;
                    foreach ($custom_form_item_master as $b_i_row) 
                    {
                        $custom_form_item_bucket[] = array(
                            'no' => $i,
                            'task_name' => $b_i_row->task_name,
                            'task_description' => $b_i_row->task_description,
                            'task_completion_days' => $b_i_row->task_completion_days,
                            'task_date' => $b_i_row->task_date,
                            'actual_start_date' => $b_i_row->actual_start_date,
                            'actual_completion_date' => $b_i_row->actual_completion_date,
                            'task_status_enum_id' => $b_i_row->task_status_enum_id,
                            'assigned_to' => $b_i_row->assigned_to,
                            'branch_id' => $b_i_row->branch_id,
                        );
                        $i++;
                    }
                }

                if (!empty($custom_form_user_mapper_master)) 
                {
                    $i = 1;
                    foreach ($custom_form_user_mapper_master as $b_i_row) 
                    {
                        $custom_form_users_bucket[] = array(
                            'no' => $i,
                            'id' => $b_i_row->id,
                            'custom_form_id' => $b_i_row->custom_form_id,
                            'user_id' => $b_i_row->user_id,
                            'branch_id' => $b_i_row->branch_id,
                        );
                        $i++;
                    }
                }

            
                $created_date = date_create($r->created_date);
                $created_date_format = date_format($created_date,"d-M-Y");

                //get actual_cost from purchase invoice and expense and total_revenue from sale invoice
                //in invoice_reference_master by custom_form_id
                $custom_form_bucket[] = array(
                    'custom_form_id' => $r->custom_form_id,
                    'custom_form_name' => $r->custom_form_name,
                    'custom_form_date' => $r->custom_form_date,
                    'expected_start_date' => $r->expected_start_date,
                    'expected_end_date' => $r->expected_end_date,
                    'custom_form_status_enum_id' => $r->custom_form_status_enum_id,
                    'custom_form_priority_enum_id' => $r->custom_form_priority_enum_id,
                    'department_enum_id' => $r->department_enum_id,
                    'customer_id' => $r->customer_id,
                    'customer_details' => $r->customer_details,
                    'estimated_cost' => $r->estimated_cost,
                    // 'actual_cost' => $r->actual_cost,
                    'branch_id' => $r->branch_id,
                    'custom_form_items' => $custom_form_item_bucket,
                    'custom_form_users' => $custom_form_users_bucket,
                    'created_date' => $created_date_format,
                );
                

            }
            $this->response($custom_form_bucket,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        } 
    }
    public function get_custom_form_profile_details_post($customer_id=NULL)
    {
        if(authorize("DV_CUSTOMER,ROLE_ADMIN",$this->api_token)) {
            $Return = array('result'=>'', 'error'=>'');
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $company_id = $user_master[0]->company_id;
            $is_progress_only = $this->input->post('is_progress_only');
            if($customer_id == NULL || !is_numeric($customer_id))
            {
                $Return['error'] = 'Invalid Customer in custom_form!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            else
            {
                if($is_progress_only == NULL)
                {
                    $custom_form_master = $this->_get_where_custom('company_id = "'.$company_id.'" AND customer_id = ',$customer_id)->result();  
                }
                else
                {
                    $custom_form_master = $this->_get_where_custom('custom_form_status_enum_id = (SELECT enum_master.enum_id FROM enum_master WHERE enum_master.type = "PROJECT STATUS" AND enum_master.value = "IN PROGRESS") AND company_id = "'.$company_id.'" AND customer_id = ',$customer_id)->result();  
                }
            }
            $custom_form_bucket = array();
            $custom_form_item_bucket = array();
            $custom_form_users_bucket = array();
            if (!empty($custom_form_master)) 
            {
                foreach ($custom_form_master as $r) 
                {
                    $created_date = date_create($r->created_date);
                    $created_date_format = date_format($created_date,"d-M-Y");

                    //get actual_cost from purchase invoice and expense and total_revenue from sale invoice
                    //in invoice_reference_master by custom_form_id
                    $custom_form_bucket[] = array(
                        'custom_form_id' => $r->custom_form_id,
                        'custom_form_name' => $r->custom_form_name,
                        'custom_form_date' => $r->custom_form_date,
                        'expected_start_date' => $r->expected_start_date,
                        'expected_end_date' => $r->expected_end_date,
                        'custom_form_status_enum_id' => $r->custom_form_status_enum_id,
                        'custom_form_status' => !empty($this->mdl_enum->get_where($r->custom_form_status_enum_id)->result())?$this->mdl_enum->get_where($r->custom_form_status_enum_id)->result()[0]->value:"",
                        'custom_form_priority_enum_id' => $r->custom_form_priority_enum_id,
                        'custom_form_priority' => !empty($this->mdl_enum->get_where($r->custom_form_priority_enum_id)->result())?$this->mdl_enum->get_where($r->custom_form_priority_enum_id)->result()[0]->value:"",
                        'department_enum_id' => $r->department_enum_id,
                        'customer_id' => $r->customer_id,
                        'customer_details' => $r->customer_details,
                        'estimated_cost' => $r->estimated_cost,
                        // 'actual_cost' => $r->actual_cost,
                        'branch_id' => $r->branch_id,
                        'custom_form_items' => $custom_form_item_bucket,
                        'custom_form_users' => $custom_form_users_bucket,
                        'created_date' => $created_date_format,
                    );
                }

            
                
                

            }
            $this->response($custom_form_bucket,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        } 
    }
    public function add_custom_form_post($id=NULL,$hash=NULL)
    {
        if(authorize("A_PROJECT,ROLE_ADMIN",$this->api_token) || password_verify('1xhV7dDR5Z', $hash)) {
            $this->db->trans_strict(TRUE);
            $this->db->trans_begin();
            $Return = array('result'=>'', 'error'=>'', 'custom_form_id'=>'');
            $error = array();
            $custom_form_data = array();
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $company_id = $user_master[0]->company_id;
            $branch_id = $this->input->post('branch_id');
            if ($branch_id == NULL) 
            {
                $Return['error'] = 'Invalid Branch!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $branch_master = $this->db->where('branch_id = '.$this->db->escape($branch_id).' AND company_id = '.$this->db->escape($company_id).'')->get('branch_master')->result();
            if (empty($branch_master)) 
            {
                $Return['error'] = 'Invalid Branch!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
      
            if($this->input->post('custom_form_name',TRUE)==='') {
                $Return['error'] = "Add Custom Form Name.";
                $error[] = 'error';
            } else if($this->input->post('custom_form_date')==='') {
                $Return['error'] = "Invalid Custom Form Date!";
            } else if(!validate_date($this->input->post('custom_form_date'),'Y-m-d')) {
                $Return['error'] = "Invalid Custom Form Date!";
            } else if($this->input->post('expected_start_date')==='') {
                $Return['error'] = "Invalid Expected Start Date!";
            } else if(!validate_date($this->input->post('expected_start_date'),'Y-m-d')) {
                $Return['error'] = "Invalid Expected Start Date!";
            } else if($this->input->post('expected_end_date')==='') {
                $Return['error'] = "Invalid Expected End Date!";
            } else if(!validate_date($this->input->post('expected_end_date'),'Y-m-d')) {
                $Return['error'] = "Invalid Expected End Date!";
            } else if(!in_array($this->input->post('custom_form_status_enum_id'), array_column($this->mdl_enum->get_where_custom('type','PROJECT STATUS')->result_array(), 'enum_id'))) {
                $Return['error'] = "Invalid custom_form status";
                $error[] = 'error';
            } else if(!in_array($this->input->post('custom_form_priority_enum_id'), array_column($this->mdl_enum->get_where_custom('type','PROJECT PRIORITY')->result_array(), 'enum_id'))) {
                $Return['error'] = "Invalid custom_form priority";
                $error[] = 'error';
            } else if(!in_array($this->input->post('department_enum_id'), array_column($this->mdl_enum->get_where_custom('type','DEPARTMENT')->result_array(), 'enum_id'))) {
                $Return['error'] = "Invalid department";
                $error[] = 'error';
            } else if($this->input->post('customer_id')===NULL || empty($this->mdl_customer->get_where_custom('company_id = "'.$company_id.'" AND customer_id = ',$this->input->post('customer_id'))->result())) {
                $Return['error'] = "Invalid customer!";
                $error[] = 'error';
            } else if($this->input->post('estimated_cost')==='' || !is_numeric($this->input->post('estimated_cost'))) {
                $Return['error'] = "Invalid estimated cost!";
                $error[] = 'error';
            } /* else if($this->input->post('actual_cost')==='' || !is_numeric($this->input->post('actual_cost'))) {
                $Return['error'] = "Invalid actual cost!";
                $error[] = 'error';
            }  */else if(!is_array($this->input->post('task_name',TRUE)) || empty($this->input->post('task_name',TRUE))){
                $Return['error'] = "Add Atleast One Task.";
                $error[] = 'error';
            } else if(is_array($this->input->post('users',TRUE)) && !empty($this->input->post('users',TRUE))){
                //users to assign to this custom_form
                $users_cnt = count($this->input->post('users',TRUE));
                for ($i=0; $i < $users_cnt; $i++) 
                { 
                    if(!is_numeric($this->input->post('users',TRUE)[$i])) {
                        $Return['error'] = "Invalid User.";
                        $error[] = 'error';
                    } else if($this->input->post('users',TRUE)[$i] == $this->api_token->user_id){
                        unset($this->input->post('users',TRUE)[$i]);
                    }
                }
            } 
            
            if(!empty($this->input->post('task_name',TRUE))){
                $task_name_cnt = count($this->input->post('task_name',TRUE));
                for ($i=0; $i < $task_name_cnt; $i++) 
                { 
                    if($this->input->post('task_name',TRUE)[$i]==='') {
                        $Return['error'] = "Add Task.";
                        $error[] = 'error';
                    } else if($this->input->post('task_status_enum_id',TRUE)[$i]==='') {
                        $Return['error'] = "Select Task Status.";
                        $error[] = 'error';
                    } else if(!in_array($this->input->post('task_status_enum_id')[$i], array_column($this->mdl_enum->get_where_custom('type','TASK STATUS')->result_array(), 'enum_id'))) {
                        $Return['error'] = "Invalid task status";
                        $error[] = 'error';
                    }/* else if($this->input->post('task_date')[$i]==='') {
                        $Return['error'] = "Invalid Task Date!";
                    } else if(!validate_date($this->input->post('task_date')[$i],'Y-m-d')) {
                        $Return['error'] = "Invalid Task Date!";
                    } else if($this->input->post('actual_start_date')[$i]==='') {
                        $Return['error'] = "Invalid Actual Start Date!";
                    } else if(!validate_date($this->input->post('actual_start_date')[$i],'Y-m-d')) {
                        $Return['error'] = "Invalid Actual Start Date!";
                    } else if($this->input->post('actual_completion_date')[$i]==='') {
                        $Return['error'] = "Invalid Actual Completion Date!";
                    } else if(!validate_date($this->input->post('actual_completion_date')[$i],'Y-m-d')) {
                        $Return['error'] = "Invalid Actual Completion Date!";
                    } else if($this->input->post('task_completion_days',TRUE)[$i]==='' || !is_numeric($this->input->post('task_completion_days',TRUE)[$i])) {
                        $Return['error'] = "Add Task Completion Days.";
                        $error[] = 'error';
                    }  */ 
                }

            }

            $users = $this->input->post('users',TRUE);
            $get_all_admins = $this->mdl_user->get_admin_by_company($company_id,$this->api_token->user_id)->result_array();
            $array_diff = array_diff(array_column($get_all_admins,'user_id'),$users);
            if(!empty($array_diff))
            {
                $users = array_merge($users,$array_diff);    
            }

            if($Return['error']!=''){
                $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
            }
            $success_msg = 'Custom Form Created Successfully!';
            if (password_verify('1xhV7dDR5Z', $hash)) 
            {
                /* if($this->input->post('custom_form_name',TRUE) != NULL)
                {
                    //check duplicate template name
                    $custom_form_master = $this->_get_where_custom('custom_form_name = "'.$this->input->post('custom_form_name',TRUE).'" AND custom_form_id != "'.$id.'" AND company_id=',$company_id)->result();
                    if(!empty($custom_form_master))
                    {
                        $Return['error'] = "Duplicate Custom Form Name!";
                        $error[] = 'error';
                        $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
                    }

                } */
                $custom_form_user_result = $this->mdl_custom_form_user_mapper->get_where_custom('user_id = "'.$this->api_token->user_id.'" AND custom_form_id =',$id)->result();
                if (empty($custom_form_user_result)) 
                {
                    $Return['error'] = "You are not allowed to update this custom_form.";
                    $error[] = 'error';
                    $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
                }

                //call delete function
                $this->delete_custom_form_post($id,$hash); 

                //update custom_form_master
                $custom_form_data = array(
                    'custom_form_name' => $this->input->post('custom_form_name',TRUE),
                    'custom_form_date' => $this->input->post('custom_form_date',TRUE),
                    'expected_start_date' => $this->input->post('expected_start_date',TRUE),
                    'expected_end_date' => $this->input->post('expected_end_date',TRUE),
                    'custom_form_status_enum_id' => $this->input->post('custom_form_status_enum_id',TRUE),
                    'custom_form_priority_enum_id' => $this->input->post('custom_form_priority_enum_id',TRUE),
                    'department_enum_id' => $this->input->post('department_enum_id',TRUE),
                    'customer_details' => $this->input->post('customer_details',TRUE),
                    'customer_id' => $this->input->post('customer_id',TRUE),
                    'estimated_cost' => $this->input->post('estimated_cost',TRUE),
                    // 'actual_cost' => $this->input->post('actual_cost',TRUE),
                    'branch_id' => $this->input->post('branch_id',TRUE),
                    'updated_date' =>  date('Y-m-d H:i:s'),
                    'updated_by' =>  $this->api_token->user_id,
                );
                $custom_form_result = $this->_update($id,$custom_form_data);
                $success_msg = 'Custom Form Updated Successfully!';
            }
            else
            {
                /* if($this->input->post('custom_form_name',TRUE) != NULL)
                {
                    //check duplicate template name
                    $custom_form_master = $this->_get_where_custom('custom_form_name = "'.$this->input->post('custom_form_name',TRUE).'" AND company_id=',$company_id)->result();
                    if(!empty($custom_form_master))
                    {
                        $Return['error'] = "Duplicate Custom Form Name!";
                        $error[] = 'error';
                        $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
                    }

                } */
                //insert custom_form_master
                $custom_form_data = array(
                    'custom_form_name' => $this->input->post('custom_form_name',TRUE),
                    'custom_form_date' => $this->input->post('custom_form_date',TRUE),
                    'expected_start_date' => $this->input->post('expected_start_date',TRUE),
                    'expected_end_date' => $this->input->post('expected_end_date',TRUE),
                    'custom_form_status_enum_id' => $this->input->post('custom_form_status_enum_id',TRUE),
                    'custom_form_priority_enum_id' => $this->input->post('custom_form_priority_enum_id',TRUE),
                    'department_enum_id' => $this->input->post('department_enum_id',TRUE),
                    'customer_id' => $this->input->post('customer_id',TRUE),
                    'estimated_cost' => $this->input->post('estimated_cost',TRUE),
                    // 'actual_cost' => 0,//$this->input->post('actual_cost',TRUE),
                    'branch_id' => $this->input->post('branch_id',TRUE),
                    'company_id' => $company_id,
                    'parent_company_id' => $company_id,
                    'created_date' =>  date('Y-m-d H:i:s'),
                    'updated_date' =>  date('Y-m-d H:i:s'),
                    'created_by' =>  $this->api_token->user_id,
                );
                if ($this->input->post('customer_details',TRUE) != '') 
                {
                    $custom_form_data['customer_details'] = $this->input->post('customer_details',TRUE);
                }
                $custom_form_result = $this->_insert($custom_form_data);
            }
            
            
            if ($custom_form_result != NULL) {
                
                //insert custom_form_user_mapper
                $custom_form_user_mapper_result = NULL;
                $custom_form_user_mapper_data = array();
                $custom_form_user_mapper_data[] = array(
                    'custom_form_id' => $custom_form_result,
                    'branch_id' => $this->input->post('branch_id',TRUE),
                    'company_id' => $company_id,
                    'parent_company_id' => $company_id,
                    'created_date' =>  date('Y-m-d H:i:s'),
                    'updated_date' =>  date('Y-m-d H:i:s'),
                    'user_id' =>  $this->api_token->user_id,
                );
                if(is_array($users) && !empty($users))
                {
                    for ($i=0; $i < count($users); $i++) 
                    { 
                        $custom_form_user_mapper_data[] = array(
                            'custom_form_id' => $custom_form_result,
                            'branch_id' => $this->input->post('branch_id',TRUE),
                            'company_id' => $company_id,
                            'parent_company_id' => $company_id,
                            'created_date' =>  date('Y-m-d H:i:s'),
                            'updated_date' =>  date('Y-m-d H:i:s'),
                            'user_id' =>  $users[$i],
                        );
                    }
                }

                if (!empty($custom_form_user_mapper_data)) 
                {
                    //insert the custom_form_user_mapper
                    $custom_form_user_mapper_result = $this->mdl_custom_form_user_mapper->_insert_batch($custom_form_user_mapper_data);
        
                    if ($custom_form_user_mapper_result == NULL) {
                        $Return['error'] = "!!Something went wrong!!";
                        $error[] = 'error';
                    }
                    
                }
                
                
                //insert custom_form_item_master
                $custom_form_item_result = NULL;
                $custom_form_item_data = array();
                for ($i=0; $i < count($this->input->post('task_name',TRUE)); $i++) 
                { 
                    if($this->input->post('assigned_to',TRUE)[$i] != '' && is_numeric($this->input->post('assigned_to',TRUE)[$i]) && !empty($this->mdl_user->get_where_custom('company_id = "'.$company_id.'" AND user_id = ',$this->input->post('assigned_to',TRUE)[$i])->result()))
                    {
                        $assigned_to = $this->input->post('assigned_to',TRUE)[$i];
                    }
                    else
                    {
                        $assigned_to = $this->api_token->user_id;
                    }

                    $task_date = NULL;
                    $actual_start_date = NULL;
                    $actual_completion_date = NULL;
                    $task_completion_days = NULL;
                    if($this->input->post('task_date')[$i]!=='' && validate_date($this->input->post('task_date')[$i],'Y-m-d')) {
                        $task_date = $this->input->post('task_date')[$i];
                    }
                    if($this->input->post('actual_start_date')[$i]!=='' && validate_date($this->input->post('actual_start_date')[$i],'Y-m-d')) {
                        $actual_start_date = $this->input->post('actual_start_date')[$i];
                    }
                    if($this->input->post('actual_completion_date')[$i]!=='' && validate_date($this->input->post('actual_completion_date')[$i],'Y-m-d')) {
                        $actual_completion_date = $this->input->post('actual_completion_date')[$i];
                    }
                    if($this->input->post('task_completion_days')[$i]!=='' && is_numeric($this->input->post('task_completion_days',TRUE)[$i])) {
                        $task_completion_days = $this->input->post('task_completion_days')[$i];
                    }
               
                    $custom_form_item_data[] = array(
                        'custom_form_id' => $custom_form_result,
                        'task_name' => $this->input->post('task_name',TRUE)[$i],
                        'task_description' => $this->input->post('task_description',TRUE)[$i],
                        'task_date' => $task_date,
                        'actual_start_date' => $actual_start_date,
                        'actual_completion_date' => $actual_completion_date,
                        'task_status_enum_id' => $this->input->post('task_status_enum_id',TRUE)[$i],
                        'task_completion_days' => $task_completion_days,
                        'assigned_to' => $assigned_to,
                        'branch_id' => $this->input->post('branch_id',TRUE),
                        'company_id' => $company_id,
                        'parent_company_id' => $company_id,
                        'created_date' =>  date('Y-m-d H:i:s'),
                        'updated_date' =>  date('Y-m-d H:i:s'),
                        'created_by' =>  $this->api_token->user_id,
                    );
                }

                if (!empty($custom_form_item_data)) 
                {
                    $duplicate_task_name = array_diff_assoc(array_column($custom_form_item_data,'task_name'), array_unique(array_column($custom_form_item_data,'task_name')));

                    if(empty($duplicate_task_name))
                    {
                        //insert the custom_form_item
                        $custom_form_item_result = $this->mdl_custom_form_item->_insert_batch($custom_form_item_data);
           
                        if ($custom_form_item_result != NULL) {
                            $Return['result'] = $success_msg;
                        } else {
                            $Return['error'] = "!!Something went wrong!!";
                            $error[] = 'error';
                        }
                    }
                    else
                    {
                        $Return['error'] = "Duplicate task name!";
                        $error[] = 'error';
                    }
                    
                }


                
                
            } else {
                $Return['error'] = "!!Something went wrong!!";
                $error[] = 'error';
            }


            // $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE || !empty($error))
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
            }
            if ($custom_form_result != NULL) 
            {
                $Return['custom_form_id'] = $custom_form_result;
            }
            $this->response($Return,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }

    public function return_custom_form_name_post($id=NULL)
    {
        if(authorize("A_SALES_ORDER,U_SALES_ORDER,A_SALES_INVOICE,U_SALES_INVOICE,A_EXPENSE,U_EXPENSE,A_PURCHASE_INVOICE,U_PURCHASE_INVOICE,A_PURCHASE_ORDER,U_PURCHASE_ORDER,ROLE_ADMIN",$this->api_token)) {
            $Return = array('result'=>'', 'error'=>'');
            $search = $this->input->post('search');
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $company_id = $user_master[0]->company_id;
            if($search == NULL && $id == NULL)
            {
                $Return['error'] = 'Invalid Custom Form!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            
            if($id != NULL && is_numeric($id))
            {
                $condition = 'custom_form_id = '.$this->db->escape($id).' AND company_id = ';
                $custom_form_master = $this->mdl_custom_form->get_where_custom($condition,$company_id)->result();
            }
            else
            {
                $condition = 'company_id = '.$company_id;
                $custom_form_master = $this->mdl_custom_form->get_custom_form_like($search,$condition)->result();
            }
            
          
            $custom_form_bucket = array();
            if (!empty($custom_form_master)) 
            {
                $i = 1;
                foreach ($custom_form_master as $b_i_row) 
                {
                    $custom_form_bucket[] = array(
                        'no' => $i,
                        'id' => $b_i_row->custom_form_id,
                        'value' => $b_i_row->custom_form_id,
                        'custom_form_name' => $b_i_row->custom_form_name,
                        'label' => $b_i_row->custom_form_name,
                        'branch_id' => $b_i_row->branch_id,
                    );
                    $i++;
                }
            }

            $this->response($custom_form_bucket,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        } 
    }

    public function update_custom_form_post($id=NULL)
    {
        if(authorize("E_PROJECT,ROLE_ADMIN",$this->api_token)) {
            $Return = array('result'=>'', 'error'=>'');
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if ($id == NULL || !is_numeric($id)) 
            {
                $Return['error'] = 'Invalid Custom Form!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $company_id = $user_master[0]->company_id;
            $branch_id = $this->input->post('branch_id');
            if ($branch_id == NULL) 
            {
                $Return['error'] = 'Invalid Branch!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $branch_master = $this->db->where('branch_id = '.$this->db->escape($branch_id).' AND company_id = '.$this->db->escape($company_id).'')->get('branch_master')->result();
            if (empty($branch_master)) 
            {
                $Return['error'] = 'Invalid Branch!';
                $this->response($Return,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            $ids = $this->mdl_custom_form->get_custom_form_ids($company_id);
            if (!in_array($id, $ids)) 
            {
                $Return['error'] = 'Invalid Custom Form!';
                $this->response($Return,MY_Controller::HTTP_BAD_REQUEST);
            }

            $hash = password_hash("1xhV7dDR5Z", PASSWORD_DEFAULT);
            
            $this->add_custom_form_post($id,$hash);


        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }

    private function get_custom_form_data_post()
    {
        $custom_form_bucket = array();
        $user_id = $this->input->post('token_user_id',TRUE);
        if ($user_id != '' && is_numeric($user_id) || $user_id != NULL && is_numeric($user_id)) 
        {
            $custom_form_master = $this->_get_where_custom('tally_alter = 1 OR tally_sync =',0)->result();
            $custom_form_bucket = array();
            if (!empty($custom_form_master)) 
            {
                foreach ($custom_form_master as $data) 
                {
                    $this->load->model('product/mdl_product','mdl_product');
                    $product_name = !empty($this->mdl_product->get_where($data->product_id)->result())?$this->mdl_product->get_where($data->product_id)->result()[0]->description:'<span style="color:red">PURGED</span>';
                    $custom_form_bucket[] = array(
                        'custom_form_id' => $data->id,
                        'custom_form_number' => $data->custom_form_number,
                        'product_name' => $product_name,
                        'tally_sync' => $data->tally_sync,
                        'tally_alter' => $data->tally_alter,
                        'tally_product_hide' => $data->tally_product_hide,
                    );
                }
                

            }
            $this->response($custom_form_bucket,MY_Controller::HTTP_OK);
        }
        $this->response($custom_form_bucket,MY_Controller::HTTP_OK);
        
        
    }

    private function _custom_form_history_save($custom_form_id)
    {
        $custom_form_master = $this->mdl_custom_form->get_where($custom_form_id)->result()[0];
        $current_timestamp = strtotime("now");
        $custom_form_data = array();
        $custom_form_item_bucket = array();

        


        $custom_form_data[$current_timestamp] = array(
            'custom_form_id' => $custom_form_master->custom_form_id,	
            'custom_form_date' => $custom_form_master->custom_form_date,	
            'custom_form_name' => $custom_form_master->custom_form_name,	
            'expected_start_date' => $custom_form_master->expected_start_date,	
            'expected_end_date' => $custom_form_master->expected_end_date,	
            'custom_form_status_enum_id' => $custom_form_master->custom_form_status_enum_id,	
            'custom_form_priority_enum_id' => $custom_form_master->custom_form_priority_enum_id,	
            'department_enum_id' => $custom_form_master->department_enum_id,	
            'customer_id' => $custom_form_master->customer_id,	
            'customer_details' => $custom_form_master->customer_details,	
            'estimated_cost' => $custom_form_master->estimated_cost,	
            // 'actual_cost' => $custom_form_master->actual_cost,	
            'branch_id' => $custom_form_master->branch_id,	
            'company_id' => $custom_form_master->company_id,	
            'parent_company_id' => $custom_form_master->parent_company_id,	
            'created_by' => $custom_form_master->created_by,	
            'created_date' => $custom_form_master->created_date,	
            'updated_by' => $custom_form_master->updated_by,	
            'updated_date' => $custom_form_master->updated_date,	
            'version' => $custom_form_master->version,	
            'deleted' => $custom_form_master->deleted,

        );

        $custom_form_item_master = $this->mdl_custom_form_item->get_where_custom('custom_form_id',$custom_form_id)->result();
        


        if (!empty($custom_form_item_master)) 
        {
            foreach ($custom_form_item_master as $custom_form_item) 
            {
                $custom_form_item_bucket[] = array(    
                    'id' => $custom_form_item->id,	
                    'custom_form_id' => $custom_form_item->custom_form_id,	
                    'task_name' => $custom_form_item->task_name,	
                    'task_description' => $custom_form_item->task_description,	
                    'task_completion_days' => $custom_form_item->task_completion_days,	
                    'task_date' => $custom_form_item->task_date,	
                    'actual_start_date' => $custom_form_item->actual_start_date,	
                    'actual_completion_date' => $custom_form_item->actual_completion_date,	
                    'task_status_enum_id' => $custom_form_item->task_status_enum_id,	
                    'assigned_to' => $custom_form_item->assigned_to,	
                    'branch_id' => $custom_form_item->branch_id,	
                    'company_id' => $custom_form_item->company_id,	
                    'parent_company_id' => $custom_form_item->parent_company_id,	
                    'created_by' => $custom_form_item->created_by,	
                    'created_date' => $custom_form_item->created_date,	
                    'updated_by' => $custom_form_item->updated_by,	
                    'updated_date' => $custom_form_item->updated_date,	
                    'version' => $custom_form_item->version,	
                    'deleted' => $custom_form_item->deleted,	
                );
            }
            $custom_form_data[$current_timestamp]['custom_form_item_master'] = $custom_form_item_bucket;

        }

        
        if ($custom_form_data != '') 
        {
            if($custom_form_master->custom_form_history != '')
            {
                $json_data = json_encode(json_decode($custom_form_master->custom_form_history, true)+$custom_form_data);
            }
            else
            {
                $json_data = json_encode($custom_form_data);
            }
            $update_history_data = array(
                'custom_form_history' => $json_data,
            );
            $custom_form_update = $this->mdl_custom_form->_update($custom_form_id,$update_history_data);
            if ($custom_form_update != NULL) 
            {
                return $custom_form_master;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    private function get_custom_form_data_id_post()
    {
        $custom_form_bucket = array();
        $user_id = $this->input->post('token_user_id',TRUE);
        if ($user_id != '' && is_numeric($user_id) || $user_id != NULL && is_numeric($user_id)) 
        {
            $id = $this->input->post('custom_form_id',TRUE);
            $custom_form_master = $this->_get_where_custom('tally_alter = 1 AND id = "'.$id.'" OR tally_sync = '.$this->db->escape(0).' AND id =',$id)->result();
            $custom_form_bucket = array();
            $custom_form_item_bucket = array();
            $total_amount = 0.00;
            $rate = 0.00;
            $first_item_godown = '';
            if (!empty($custom_form_master)) 
            {
                $r = $custom_form_master[0];
                $this->load->model('uom/mdl_uom','mdl_uom');
                $this->load->model('enum/mdl_enum','mdl_enum');
                $this->load->model('product/mdl_product','mdl_product');
                $this->load->model('product/mdl_product_model','mdl_product_model');
                $this->load->model('sales_order/mdl_sales_order','mdl_sales_order');
                $this->load->model('mdl_custom_form_item');
                
                

                $custom_form_item_master = $this->mdl_custom_form_item->get_where_custom('custom_form_id',$r->custom_form_id)->result();
                if (!empty($custom_form_item_master)) 
                {
                    $custom_form_item_bucket = array();
                    $i=1;
                    foreach ($custom_form_item_master as $b_i_row) 
                    {
                        $product = !empty($this->mdl_product->get_where($b_i_row->part_id)->result())?$this->mdl_product->get_where($b_i_row->part_id)->result()[0]->description:'';
                        $uom = !empty($this->mdl_product->get_where($b_i_row->part_id)->result())?$this->mdl_uom->get_where($this->mdl_product->get_where($b_i_row->part_id)->result()[0]->uom_id)->result()[0]->name:'';

                        $part_number = !empty($this->mdl_product->get_where($b_i_row->part_id)->result())?$this->mdl_product->get_where($b_i_row->part_id)->result()[0]->part_number:'';
                        $godown = !is_null($b_i_row->godown_id) && !empty($this->mdl_enum->get_where($b_i_row->godown_id)->result())?$this->mdl_enum->get_where($b_i_row->godown_id)->result()[0]->value:'';

                        if ($i==1) 
                        {
                            //first item godown
                            $first_item_godown = $godown;
                        }

                        $custom_form_item_bucket[] = array(
                            'bti_id' => $b_i_row->id,
                            'part_id' => $b_i_row->part_id,
                            'part_name' => $product,
                            'uom' => $uom,
                            'part_number' => $part_number,
                            'godown_id' => $b_i_row->godown_id,
                            'godown_name' => $godown,
                            'quantity' => $b_i_row->quantity,
                            'rate' => $b_i_row->rate,
                            'amount' => $b_i_row->amount,
                        );
                        $total_amount += $b_i_row->amount;
                        $i++;
                    }
                }

                $sales_order_name = !is_null($r->sales_order_id) && !empty($this->mdl_sales_order->get_where($r->sales_order_id)->result())?$this->mdl_sales_order->get_where($r->sales_order_id)->result()[0]->sales_order_number:'';

                $product_name = !empty($this->mdl_product->get_where($r->product_id)->result())?$this->mdl_product->get_where($r->product_id)->result()[0]->description:'';

                $product_model_name = !empty($this->mdl_product->get_where($r->product_id)->result())?$this->mdl_product->get_where($r->product_id)->result()[0]->description.' '.$this->mdl_product_model->get_where($r->product_model_id)->result()[0]->model_name:'';

                $uom_name = !empty($this->mdl_product->get_where($r->product_id)->result())?$this->mdl_uom->get_where($this->mdl_product->get_where($r->product_id)->result()[0]->uom_id)->result()[0]->name:'';

                $godown_name = !is_null($r->godown_id) && !empty($this->mdl_enum->get_where($r->godown_id)->result())?$this->mdl_enum->get_where($r->godown_id)->result()[0]->value:'';

                if ($total_amount > 0 && $r->quantity > 0) 
                {
                    $rate = number_format(floatval($total_amount)/floatval($r->quantity),2);
                }
                $created_date = date_create($r->custom_form_date);
                $created_date_format = date_format($created_date,"Ymd");
                $custom_form_date_another_format = date_format($created_date,"d-M-Y");

                $this->load->model('voucher/mdl_voucher','mdl_voucher');

                $voucher_name = '';
                
                $voucher_master = $this->mdl_voucher->get_where($r->voucher_id)->result();

                if (!empty($voucher_master)) 
                {
                    $voucher_name = $voucher_master[0]->voucher_name;
                }
                $this->load->model('register/mdl_register','mdl_register');
                $user_master = $this->mdl_register->get_where($this->api_token->user_id)->result()[0];

                $this->load->model('tally_settings/mdl_tally_settings','mdl_tally_settings');
                $tally_settings = $this->mdl_tally_settings->get_where(1)->result();
                if (empty($tally_settings)) 
                {
                    $Return['error'] = "Invalid Tally Settings!";
                    $this->response($Return,MY_Controller::HTTP_OK);

                }
                $custom_form_bucket[] = array(
                    'custom_form_id' => $r->custom_form_id,
                    'custom_form_number' => $r->custom_form_number,
                    'narration' => $r->narration,
                    'tag' => $r->tag,
                    'voucher_id' => $r->voucher_id,
                    'voucher_name' => $voucher_name,
                    'tally_product_hide' => $r->tally_product_hide,
                    'tally_sync' => $r->tally_sync,
                    'tally_alter' => $r->tally_alter,
                    'product_model_name' => $product_model_name,
                    'sales_order_id' => $r->sales_order_id,
                    'sales_order_name' => $sales_order_name,
                    'product_id' => $r->product_id,
                    'godown_id' => $r->godown_id,
                    'godown_name' => $godown_name,
                    'template_name' => $r->template_name,
                    'product_name' => $product_name,
                    'custom_form_quantity' => $r->quantity,
                    'custom_form_items' => $custom_form_item_bucket,
                    'total_amount' => $total_amount,
                    'rate' => $rate,
                    'created_date' => $created_date_format,
                    'custom_form_date_another_format' => $custom_form_date_another_format,
                    'uom_name' => $uom_name,
                    'first_item_godown' => $first_item_godown,
                    'company_name' => $tally_settings[0]->company_name,
                    'company_address' => $user_master->address,

                );

            }
            $this->response($custom_form_bucket,MY_Controller::HTTP_OK);
        }
        $this->response($custom_form_bucket,MY_Controller::HTTP_OK);
        
    }

    public function tally_sync_done_post()
    {
        $user_id = $this->input->post('token_user_id',TRUE);
        $custom_form_id = $this->input->post('custom_form_id',TRUE);
        $message = $this->input->post('message',TRUE);
        $status = $this->input->post('status',TRUE);
        if ($custom_form_id!=NULL && is_numeric($custom_form_id) && $user_id!=NULL && is_numeric($user_id)|| $custom_form_id!='' && is_numeric($custom_form_id) && $user_id!='' && is_numeric($user_id)) 
        {
            if ($status=='true') 
            {
                $custom_form_master_check = $this->_get_where_custom('tally_alter = 1 AND id = "'.$custom_form_id.'" OR tally_sync = '.$this->db->escape(0).' AND id =',$custom_form_id)->result();
                if (!empty($custom_form_master_check)) 
                {
                    $data = array(
                        'tally_sync' => TRUE,
                        'tally_alter' => 0,
                        'tally_sync_date' => date('Y-m-d H:i:s')
                    );
                    $update_custom_form = $this->_update($custom_form_id,$data);
                    if ($update_custom_form != NULL) 
                    {
                        $this->response($message,MY_Controller::HTTP_OK);
                    }
                    else
                    {
                        $this->response('Something Went Wrong!!',MY_Controller::HTTP_OK);
                    }

                }
                else
                {
                    $this->response('Invalid Custom Form!!',MY_Controller::HTTP_OK);

                }            
            }
            else
            {
                $this->response($message,MY_Controller::HTTP_OK);

            }

        }
        
    }

    public function return_custom_form_cost_revenue_post($id=NULL)
    {
        if(authorize("LV_PROJECT,A_PROJECT,E_PROJECT,ROLE_ADMIN",$this->api_token)) {
            $user_master = $this->db->where('user_id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            $company_id = $user_master[0]->company_id;

            // $branch_id = $this->input->post('branch_id',TRUE);
            // if ($branch_id == NULL || !is_numeric($branch_id)) 
            // {
            //     $branch_id = NULL;
            // }

            $result = array();

            $result = $this->mdl_invoice_reference->get_custom_form_cost_revenue_data($id,$company_id)->result_array();

            $this->response($result,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }

    public function delete_custom_form_post($id=NULL,$hash=NULL) 
    {
        if(authorize("D_PROJECT,ROLE_ADMIN",$this->api_token) || password_verify('1xhV7dDR5Z', $hash)) {
            $Return = array('result'=>'', 'error'=>'');
            if (password_verify('1xhV7dDR5Z', $hash)) 
            {
                $id = $id;
                $custom_form_user_result = array('hash' => $hash);
            }
            else
            {
                $this->db->trans_strict(TRUE);
                $this->db->trans_begin();
                $id = $this->input->post('_token',TRUE);
                $custom_form_user_result = $this->mdl_custom_form_user_mapper->get_where_custom('user_id = "'.$this->api_token->user_id.'" AND custom_form_id =',$id)->result();
            }
            $result = $this->_get_where($id)->result();
            
            if (!empty($result) && !empty($custom_form_user_result)) 
            {
                $pass = TRUE;
                if(!password_verify('1xhV7dDR5Z', $hash))
                {
                    //check sale,purchase,expense
                    $this->load->model('sales_order/mdl_sales_order','mdl_sales_order');
                    $this->load->model('purchase_order/mdl_purchase_order','mdl_purchase_order');
                    $invoice_reference = $this->mdl_invoice_reference->get_where_custom('custom_form_id',$id);
                    $sales_order = $this->mdl_sales_order->get_where_custom('custom_form_id',$id);
                    $purchase_order = $this->mdl_purchase_order->get_where_custom('custom_form_id',$id);
                    if(!empty($invoice_reference) || !empty($sales_order) || !empty($purchase_order))
                    {
                        $Return['error'] = "This Custom Form Is Used By Other Voucher(s), You Cannot Delete This!";
                        $pass = FALSE;
                    }
                }
                if ($pass) 
                {
                    if(password_verify('1xhV7dDR5Z', $hash))
                    {
                        $save_backup = $this->_custom_form_history_save($id);
                    }

                    //delete custom_form_users
                    $custom_form_user_mapper_delete = $this->mdl_custom_form_user_mapper->_delete_by_custom_form_id($id);
                    if($custom_form_user_mapper_delete == NULL)
                    {
                        $Return['error'] = "Error on deleting custom_form users!";
                    }
                    //delete custom_form_items
                    $custom_form_item_fetch = $this->mdl_custom_form_item->get_where_custom_custom_form_item('custom_form_id',$id)->result_array();
                    $custom_form_item_fetch_ids = array_column($custom_form_item_fetch, 'id');
                    if (!empty($custom_form_item_fetch_ids)) 
                    {
                        //remove the custom_form_item
                        $remove_custom_form_item_now = $this->mdl_custom_form_item->_delete_batch('id',$custom_form_item_fetch_ids);
                    }


                    if (!password_verify('1xhV7dDR5Z', $hash)) 
                    {
                        $custom_form = $this->_delete($id);
                        if ($custom_form != NULL) 
                        {   
                            $Return['result'] = "Successfully Deleted Record";
                        }
                        else
                        {
                            $Return['error'] = "!!Something Went Wrong!!";

                        }
                    }


                }
            }
            else{
                $Return['error'] = "Custom Form Not Exists!";

            }

            if (!password_verify('1xhV7dDR5Z', $hash)) 
            {
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE || $Return['error'] != '')
                {
                    $this->db->trans_rollback();
                }
                else
                {
                    $this->db->trans_commit();
                }
                $this->response($Return,MY_Controller::HTTP_OK);
            }
            else
            {
                return TRUE;
            }
            
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }



    // public function get_custom_form_number()
    // {
    //     if(authorize("A_PROJECT,E_PROJECT,ROLE_ADMIN",$this->api_token)) {
    //         $this->load->model('mdl_custom_form');
    //         $query = $this->mdl_custom_form->get_custom_form_number();
    //         echo $query;
    //     }
    // }




    function _lock_single_custom_form($custom_form_id)
    {
        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->lock_single_custom_form($custom_form_id);
        return $query;
    }


    function _lock_custom_form()
    {
        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->lock_custom_form();
        return $query;
    }


    function _get_custom_form_number()
    {
        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->get_custom_form_number();
        return $query;
    }


    function _get($order_by)
    {
        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->get($order_by);
        return $query;
    }

    function _get_with_limit_where($limit, $offset, $order_by, $dir, $condition) 
    {
        if ((!is_numeric($limit)) || (!is_numeric($offset))) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->get_with_limit_where($limit, $offset, $order_by, $dir, $condition);
        return $query;
    }

    function _get_with_limit($limit, $offset, $order_by, $dir) 
    {
        if ((!is_numeric($limit)) || (!is_numeric($offset))) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->get_with_limit($limit, $offset, $order_by, $dir);
        return $query;
    }

    function _get_where($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->get_where($id);
        return $query;
    }

    function _get_where_custom($col, $value) 
    {
        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->get_where_custom($col, $value);
        return $query;
    }

    function _insert($data)
    {
        $this->load->model('mdl_custom_form');
        return $this->mdl_custom_form->_insert($data);
    }


    function _update($id, $data)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_custom_form');
        return $this->mdl_custom_form->_update($id, $data);
    }

    function _delete($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_custom_form');
        return $this->mdl_custom_form->_delete($id);
    }

    function _count_where($column, $value) 
    {
        $this->load->model('mdl_custom_form');
        $count = $this->mdl_custom_form->count_where($column, $value);
        return $count;
    }

    function _count_all() 
    {
        $this->load->model('mdl_custom_form');
        $count = $this->mdl_custom_form->count_all();
        return $count;
    }

    function _get_max() 
    {
        $this->load->model('mdl_custom_form');
        $max_id = $this->mdl_custom_form->get_max();
        return $max_id;
    }

    function _custom_query($mysql_query) 
    {
        $this->load->model('mdl_custom_form');
        $query = $this->mdl_custom_form->_custom_query($mysql_query);
        return $query;
    }

}
