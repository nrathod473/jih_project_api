<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_custom_form extends CI_Model
{

function __construct() {
parent::__construct();
}

function get_table() {
    $table = "custom_form_master";
    return $table;
}

function lock_custom_form_with_id_in($values)
{
    $values = implode("','",$values);
    $this->db->query("SELECT b_m.id FROM custom_form_master b_m WHERE id IN ('".$values."') FOR UPDATE");
}

function _update_batch($data,$column){
    $table = $this->get_table();
    if ($this->db->update_batch($table,$data, $column)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

public function count_voucher_id_with_condition($condition)
{
    $this->db->where($condition);
    $this->db->from('custom_form_master');
    $this->db->select('count(id) as total');
    $query = $this->db->get();
    return $query->result()[0]->total;
}

function get($order_by){
    $table = $this->get_table();
    $this->db->order_by($order_by);
    $query=$this->db->get($table);
    return $query;
}

function get_with_limit_where($limit, $offset, $order_by, $dir,$condition) {
    $table = $this->get_table();
    $this->db->group_by($table.'.id');
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by,$dir);
    $this->db->join('custom_form_location_master',$table.'.id = custom_form_location_master.custom_form_id');
    $this->db->where($condition);
    $this->db->select($table.'.*');
    $query=$this->db->get($table);
    return $query;
}

function get_with_limit($limit, $offset, $order_by, $dir) {
    $table = $this->get_table();
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by,$dir);
    $query=$this->db->get($table);
    return $query;
}

function get_custom_form_like($search, $condition) {
    $table = $this->get_table();
    // $this->db->group_by('custom_form_name');
    $this->db->order_by('custom_form_name','asc');
    $this->db->like('custom_form_name',$search);
    $this->db->where($condition);
    $query=$this->db->get($table);
    return $query;
}

function get_where($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_custom($col, $value) {
    $table = $this->get_table();
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    return $query;
}

function _insert($data){
    $table = $this->get_table();
    
    if ($this->db->insert($table, $data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return FALSE;
    }
}

function _update($id, $data){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->update($table, $data)) 
    {
        return $id;
    }
    else
    {
        return FALSE;
    }
}

function _delete($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function lock_single_custom_form($id)
{
    $this->db->query("SELECT b_m.id as b_id FROM custom_form_master b_m WHERE b_m.id = '".$id."' FOR UPDATE");
}

function lock_custom_form()
{
    $this->db->query("SELECT b_m.id as b_id FROM custom_form_master b_m FOR UPDATE");
}
public function get_ids($company_id)
{
    $ids = array();
    $this->db->where('parent_company_id = "'.$company_id.'"');
    $this->db->from('custom_form_master');
    $this->db->select('id');
    $query = $this->db->get();
    if ($query!='') 
    {
        foreach ($query->result() as $row) 
        {
            $ids[] = $row->id;
        }
        return $ids;

    }
    else
    {
        return $ids;
    }
}
function get_custom_form_number()
{
    $this->db->from('custom_form_master');
    $this->db->limit(1, 0);
    $this->db->order_by('id','DESC');
    $query = $this->db->get();
    $rowcount = $query->num_rows();
    if ($rowcount == '') 
    {
        $rowcount = 0;
    }
    else
    {
        $rowcount = $query->result()[0]->custom_form_number;
    }
    $custom_form_number = intval($rowcount) + 1;
    return $custom_form_number;
}


function count_where($column, $value) {
    $table = $this->get_table();
    $this->db->where($column, $value);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function count_all() {
    $table = $this->get_table();
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function get_max() {
    $table = $this->get_table();
    $this->db->select_max('id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
}

}