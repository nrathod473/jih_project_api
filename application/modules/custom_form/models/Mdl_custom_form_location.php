<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_custom_form_location extends CI_Model
{

function __construct() {
parent::__construct();
}

function get_table() {
    $table = "custom_form_location_master";
    return $table;
}

function get_part_not_fetched($id) {
    $table = $this->get_table();
    $this->db->where('product_master.updated_from_tally = 0 AND custom_form_location_master.custom_form_id=',$id);
    $this->db->join('product_master',$table.'.part_id = product_master.id' ,'inner');
    $this->db->select('custom_form_location_master.custom_form_id as custom_form_id,product_master.id as part_id,product_master.part_number,product_master.version as version');
    $query=$this->db->get($table);
    return $query;
}

function _insert_batch($data){
    $table = $this->get_table();
    
    if ($this->db->insert_batch($table, $data)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function _delete_batch($column,$values){
    $table = $this->get_table();
    $this->db->where_in($column, $values);
    if ($this->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function _update_batch($data,$column){
    $table = $this->get_table();
    if ($this->db->update_batch($table,$data, $column)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


function get($order_by){
    $table = $this->get_table();
    $this->db->order_by($order_by);
    $query=$this->db->get($table);
    return $query;
}

function get_with_limit_where($limit, $offset, $order_by, $dir,$condition) {
    $table = $this->get_table();
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by,$dir);
    $this->db->where($condition);
    $query=$this->db->get($table);
    return $query;
}

function get_with_limit($limit, $offset, $order_by, $dir) {
    $table = $this->get_table();
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by,$dir);
    $query=$this->db->get($table);
    return $query;
}

function get_where($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_custom_custom_form_location($col, $value) {
    $table = $this->get_table();
    $this->db->where($col, $value);
    $this->db->select('custom_form_location_master.id,custom_form_location_master.version as version');
    $query=$this->db->get($table);
    return $query;
}

function get_where_custom($col, $value) {
    $table = $this->get_table();
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    return $query;
}

function _insert($data){
    $table = $this->get_table();
    
    if ($this->db->insert($table, $data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return FALSE;
    }
}

function _update($id, $data){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->update($table, $data)) 
    {
        return $id;
    }
    else
    {
        return FALSE;
    }
}

function _delete_by_custom_form_id($id){
    $table = $this->get_table();
    $this->db->where('custom_form_id', $id);
    if ($this->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function _delete($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function count_where($column, $value) {
    $table = $this->get_table();
    $this->db->group_by($table.'.custom_form_id');
    $this->db->join('project_master',$table.'.custom_form_id = project_master.custom_form_id' ,'inner');
    $this->db->where($column, $value);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function count_all() {
    $table = $this->get_table();
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function get_max() {
    $table = $this->get_table();
    $this->db->select_max('id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
}

}