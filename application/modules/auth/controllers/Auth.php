<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('auth/mdl_auth','mdl_auth');
    }
    public function index_get()
    {
    	$data = array(
    		"status_code" => MY_Controller::HTTP_OK,
    		"message" => "Success",
    	);
    	$this->response($data,MY_Controller::HTTP_OK);
    }
    public function index_post()
    {
    	$data = array(
    		"status_code" => MY_Controller::HTTP_OK,
    		"message" => "Success",
    	);
    	$this->response($data,MY_Controller::HTTP_OK);
    }
    public function get_auth_token_post()
    {
        if($this->api_token) 
        {
            $this->response(array('result' => 'Successfully Logged in!'),MY_Controller::HTTP_OK);
        }
        
    }
    public function empty_auth_token_post()
    {
        if($this->api_token) 
        {
            $check_active_user_session = $this->db->like('data',$this->api_token->auth_key)->get('ci_sessions')->num_rows();

            if(intval($check_active_user_session) > 1)
            {
                $update_auth = $this->mdl_auth->_update($this->api_token->auth_id,array('device_id' => intval($check_active_user_session) - 1));
                if ($update_auth != NULL) 
                {
                    $this->response(array('result' => 'Successfully Logged out!'),MY_Controller::HTTP_OK);
                }
                else
                {
                    $this->response(NULL,MY_Controller::HTTP_BAD_REQUEST);
                }
            }
            else
            {
                $update_auth = $this->mdl_auth->_update($this->api_token->auth_id,array('auth_key' => '','device_id' => ''));
                if ($update_auth != NULL) 
                {
                    $this->response(array('result' => 'Successfully Logged out!'),MY_Controller::HTTP_OK);
                }
                else
                {
                    $this->response(NULL,MY_Controller::HTTP_BAD_REQUEST);
                }
            }
            
        }
        
    }

}