<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Countries extends MY_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('security');
        $this->load->database();
    }
    
    public function get_countries_post()
    {
        if(authorize("A_CUSTOMER,E_CUSTOMER,LV_CUSTOMER,DV_CUSTOMER,ROLE_ADMIN",$this->api_token)) {
            $data = array();
            $query = $this->_get('id')->result_array();
            if(!empty($query))
            {
                $data = $query;
            }
            $this->response($data,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }
	

    function _get($order_by)
    {
        $this->load->model('mdl_countries');
        $query = $this->mdl_countries->get($order_by);
        return $query;
    }

    function _get_with_limit($limit, $offset, $order_by, $dir) 
    {
        if ((!is_numeric($limit)) || (!is_numeric($offset))) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_countries');
        $query = $this->mdl_countries->get_with_limit($limit, $offset, $order_by, $dir);
        return $query;
    }

    function _get_where($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_countries');
        $query = $this->mdl_countries->get_where($id);
        return $query;
    }

    function _get_where_custom($col, $value) 
    {
        $this->load->model('mdl_countries');
        $query = $this->mdl_countries->get_where_custom($col, $value);
        return $query;
    }

    function _insert($data)
    {
        $this->load->model('mdl_countries');
        return $this->mdl_countries->_insert($data);
    }

    function _update($id, $data)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_countries');
        return $this->mdl_countries->_update($id, $data);
    }

    function _delete($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_countries');
        return $this->mdl_countries->_delete($id);
    }

    function _count_where($column, $value) 
    {
        $this->load->model('mdl_countries');
        $count = $this->mdl_countries->count_where($column, $value);
        return $count;
    }

    function _count_all() 
    {
        $this->load->model('mdl_countries');
        $count = $this->mdl_countries->count_all();
        return $count;
    }

    function _get_max() 
    {
        $this->load->model('mdl_countries');
        $max_id = $this->mdl_countries->get_max();
        return $max_id;
    }

    function _custom_query($mysql_query) 
    {
        $this->load->model('mdl_countries');
        $query = $this->mdl_countries->_custom_query($mysql_query);
        return $query;
    }
    
}
