<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Location extends MY_Controller  
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_location','mdl_location');
    }

    public function get_location_post()
	{
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
            $data = array();
            $location_list = $this->mdl_location->get_location_list_where()->result();
            $total = $this->mdl_location->count_total();
			$output = array
			(
                "recordsTotal" => $total,
                "recordsFiltered" => $total,
                "data" => $location_list
            );
            $this->response($output,MY_Controller::HTTP_OK);
		}
	}

    public function add_location_post()
    {
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
            $userid = $this->api_token->user_id;
            $parent_id = $_POST['parent_id'];
            $level_id = $_POST['level_id'];
            $user_id = $_POST['user_id'];
            $name = $_POST['location_name'];
            
            $data = array(
                'location_name'=>$name,
                'parent_id'=>$parent_id,
                'level_id '=>$level_id,
                'user_id  '=>$user_id,
                'created_by'=>$userid,
                'updated_by'=>0,
                'version'=>1,
                'created_date' => date('Y-m-d H:i:s')
            );
            $location = $this->mdl_location->add_dept($data);
		    if($location != NULL) 
			{
				$output['result'] = "Location Succesfully Created!!";
			}
			else
			{
				$output['error'] = "Something went wrong! Plz try again";
			}
            $this->response($output,MY_Controller::HTTP_OK);

        }
    }

    public function edit_location_post()
	{
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
            $id = $_POST['location_id'];
            $data = array();
            $location_list = $this->mdl_location->get_where_custom_ids_in($id)->result();
			$output = array
			(
                "data" => $location_list
            );
            $this->response($output,MY_Controller::HTTP_OK);
		}
	}

    public function update_location_post()
    {
        if(authorize("CENTER",$this->api_token) || authorize("STATE",$this->api_token))
		{
            $id =  $_POST['location_id'];
            $location_list = $this->mdl_location->get_where_custom_ids_in($id)->result();
			$parent_id = $_POST['parent_id'];
            $level_id = $_POST['level_id'];
            $user_id = $_POST['user_id'];
			$userid = $this->api_token->user_id;
            $name = $_POST['location_name'];
           
			 $data = array(
                'location_name'=>$name,
                'parent_id'=>$parent_id,
                'level_id '=>$level_id,
                'user_id  '=>$user_id,
                'updated_by'=>$userid,
                'version'=>$location_list[0]->version+1,
                'updated_date' => date('Y-m-d H:i:s')
            );
            $department = $this->mdl_location->update_dept($id,$data);
		    if($department != NULL) 
			{
				$output['result'] = "Location Updated Succesfully!!";
				$output['data'] = $data;
			}
			else
			{
				$output['error'] = "Something went wrong! Plz try again";
			}
            $this->response($output,MY_Controller::HTTP_OK);
        }
    }

}