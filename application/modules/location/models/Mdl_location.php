<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_location extends CI_Model
{

function __construct() 
{
    parent::__construct();
}

function get_table() 
{
    $table = "location_master";
    return $table;
}

function get_location_list_where()
{
    $query = $this->db->query("SELECT um.first_name as username,lma.level_name,lm.* FROM location_master as lm 
    LEFT JOIN user_master AS um ON lm.user_id=um.id
    LEFT JOIN level_master AS lma ON lm.level_id=lma.id");
    //echo $this->db->last_query();exit;
    return $query;
}

function get_where_custom_ids_in($id)
{
    //echo $id;exit;
    $query = $this->db->query("SELECT * FROM location_master WHERE id=$id");
    return $query;
}

public function add_dept($data)
{
    if ($this->db->insert('location_master',$data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return false;
    }
}

public function update_dept($id,$data)
{
    $this->db->query('SELECT id FROM location_master WHERE id = "'.$id.'" FOR UPDATE');
    if ($this->db->where('id',$id)->update('location_master',$data)) 
    {
        return $id;
    }
    else
    {
        return false;
    }
}

public function count_total()
{
    $this->db->from('location_master');
    $query = $this->db->get();
    $rowcount = $query->num_rows();
    if ($rowcount!='') 
    {
        return $rowcount;
    }
    else
    {
        return 0;
    }
}

function get_with_limit($limit, $offset, $order_by, $dir) {
    $table = $this->get_table();
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by,$dir);
    $query=$this->db->get($table);
    return $query;
}

function get_where_condition($condition) {
    $table = $this->get_table();
    $this->db->where($condition);
    $query=$this->db->get($table);
    return $query;
}

function get_where($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_custom($col, $value) {
    $table = $this->get_table();
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    //echo $this->last_query();exit;
    return $query;
}

function _insert($data){
    $table = $this->get_table();
    
    if ($this->db->insert($table, $data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return FALSE;
    }
}

function _update($id, $data){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->update($table, $data)) 
    {
        return $id;
    }
    else
    {
        return FALSE;
    }
}


function _delete($id){
    $table = $this->get_table();
    $this->db->where('id', $id);
    if ($this->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function count_where_condition($condition) {
    $table = $this->get_table();
    $this->db->where($condition);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function count_where($column, $value) {
    $table = $this->get_table();
    $this->db->where($column, $value);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function count_all() {
    $table = $this->get_table();
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function get_max() {
    $table = $this->get_table();
    $this->db->select_max('id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
}

}