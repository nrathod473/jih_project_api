<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_voucher extends CI_Model
{

function __construct() {
parent::__construct();
}

function get_table() {
    $table = "voucher_master";
    return $table;
}

public function lock_voucher($voucher_group,$voucher_id)
{
    $this->db->query("SELECT vm.voucher_id as v_id FROM voucher_master vm WHERE vm.voucher_group_id = ".$this->db->escape($voucher_group)." AND vm.voucher_id = ".$this->db->escape($voucher_id)." FOR UPDATE");
}
public function is_voucher_exist($table,$condition)
{
    $this->db->where($condition);
    $this->db->from($table);
    $this->db->group_by('voucher_id');
    $this->db->select('voucher_id');
    $query = $this->db->get();
    if (!empty($query->result())) 
    {
        return true;
    }
    else
    {
        return false;
    }
}
public function select_max_date_with_condition($table,$field,$condition)
{
    $this->db->where($condition);
    $this->db->from($table);
    $this->db->select_max("DATE_FORMAT(DATE_ADD(".$this->db->escape($field).", INTERVAL 1 MONTH),'%Y-%m-01')",'max_date');
    $query = $this->db->get();
    return $query->result();
}
public function count_voucher_id_with_condition($table,$field,$condition)
{
    $this->db->where($condition);
    $this->db->from($table);
    $this->db->select('count('.$this->db->escape($field).') as total');
    $query = $this->db->get();
    return $query->result()[0]->total;
}
public function count_total_voucher($company_id)
{
    $this->db->where('company_id = "'.$company_id.'"');
    $this->db->from('voucher_master');
    $query = $this->db->get();
    $rowcount = $query->num_rows();
    if ($rowcount!='') 
    {
        return $rowcount;
    }
    else
    {
        return 0;
    }
}
public function get_voucher_ids($company_id)
{
    $ids = array();
    $this->db->where('parent_company_id = "'.$company_id.'"');
    $this->db->from('voucher_master');
    $this->db->select('voucher_id');
    $query = $this->db->get();
    if ($query!='') 
    {
        foreach ($query->result() as $row) 
        {
            $ids[] = $row->voucher_id;
        }
        return $ids;

    }
    else
    {
        return $ids;
    }
}
public function update_voucher($id,$data)
{
    $this->db->query('SELECT voucher_id FROM voucher_master WHERE voucher_id = "'.$id.'" FOR UPDATE');
    if ($this->db->where('voucher_id',$id)->update('voucher_master',$data)) 
    {
        return true;
    }
    else
    {
        return false;
    }
}
public function add_voucher($data)
{
    if ($this->db->insert('voucher_master',$data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return false;
    }
}
public function delete_voucher($id,$company_id)
{
    if ($this->db->where('voucher_id = "'.$id.'" AND company_id = "'.$company_id.'"')->delete('voucher_master')) 
    {
        return true;
    }
    else
    {
        return false;
    }
}


function get_with_limit($limit, $offset, $order_by, $dir) {
    $table = $this->get_table();
    $this->db->limit($limit, $offset);
    $this->db->order_by($order_by,$dir);
    $query=$this->db->get($table);
    return $query;
}

function get_where_condition($condition) {
    $table = $this->get_table();
    $this->db->where($condition);
    $query=$this->db->get($table);
    return $query;
}

function get_where($id){
    $table = $this->get_table();
    $this->db->where('voucher_id', $id);
    $query=$this->db->get($table);
    return $query;
}

function get_where_custom($col, $value) {
    $table = $this->get_table();
    $this->db->where($col, $value);
    $query=$this->db->get($table);
    return $query;
}

function _insert($data){
    $table = $this->get_table();
    
    if ($this->db->insert($table, $data)) 
    {
        return $this->db->insert_id();
    }
    else
    {
        return FALSE;
    }
}

function _update($id, $data){
    $table = $this->get_table();
    $this->db->where('voucher_id', $id);
    if ($this->db->update($table, $data)) 
    {
        return $id;
    }
    else
    {
        return FALSE;
    }
}


function _delete($id){
    $table = $this->get_table();
    $this->db->where('voucher_id', $id);
    if ($this->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function count_where_condition($condition) {
    $table = $this->get_table();
    $this->db->where($condition);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function count_where($column, $value) {
    $table = $this->get_table();
    $this->db->where($column, $value);
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function count_all() {
    $table = $this->get_table();
    $query=$this->db->get($table);
    $num_rows = $query->num_rows();
    return $num_rows;
}

function get_max() {
    $table = $this->get_table();
    $this->db->select_max('voucher_id');
    $query = $this->db->get($table);
    $row=$query->row();
    $id=$row->id;
    return $id;
}

function _custom_query($mysql_query) {
    $query = $this->db->query($mysql_query);
    return $query;
}

}