<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Voucher extends MY_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('mdl_voucher');
    }

   public function get_voucher_details_post()
   {
     if(authorize("CENTRAL",$this->api_token) || authorize("STATE",$this->api_token) || authorize("UNIT",$this->api_token)) 
     {

         $final_data = array();
         $data = array();
            //Get Current Branch
            // $branch_id = $this->session->userdata('branch_id');
            $user_master = $this->db->where('id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            // $branch_query = Model\Model_branch_mapper::where("branch_id = '".$branch_id."'")->all();
            $voucher_id = $this->input->post('voucher_id',TRUE);
            // $voucher_group = $this->input->get_post('voucher_group',TRUE);
            $voucher_group = $this->input->post('voucher_group',TRUE);
            if ($voucher_id != '' && is_numeric($voucher_id)) 
            {
                //single record if voucher_id is not empty
                $condition = array(
                    'voucher_id' => $voucher_id,
                    // 'company_id' => $company_id
                );
                $voucher_master = $this->mdl_voucher->get_where_condition($condition)->result();
                if (empty($voucher_master)) 
                {
                    $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
                }
                else
                {
                    $branch_query = array('voucher_id' => '1');
                }



            }
            else
            {
                $branch_query = array('voucher_id' => '1');
            }
            if ($voucher_group == '' || !is_numeric($voucher_group)) 
            {
                // $branch_query = NULL;
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            else
            {
                switch (intval($voucher_group)) {
                    case 10:
                        //SALES_ORDER
                        if(!authorize("LV_SALES_ORDER,A_SALES_ORDER,U_SALES_ORDER,ROLE_ADMIN",$this->api_token)) 
                        {
                            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
                        }
                        else
                        {
                            $this->load->model('custom_form/mdl_custom_form');
                            $custom_form_master = $this->mdl_custom_form;

                        }
                        break;
                    case 11:
                        //BOM
                        if(!authorize("LV_BOM,A_BOM,E_BOM,ROLE_ADMIN",$this->api_token)) 
                        {
                            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
                        }
                        else
                        {
                            $this->load->model('bom/mdl_bom');
                            $bom_master = $this->mdl_bom;

                        }
                        break;
                    case 12:
                        //EMD
                        if(!authorize("LV_EMD,A_EMD,U_EMD,ROLE_ADMIN",$this->api_token)) 
                        {
                            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
                        }
                        else
                        {
                            $this->load->model('emd/mdl_emd');
                            $emd_master = $this->mdl_emd;

                        }
                        break;
                    case 13:
                        //CAPITAL_GOODS_SERVICE
                        if(!authorize("LV_CAPITAL_GOODS_SERVICE,A_CAPITAL_GOODS_SERVICE,E_CAPITAL_GOODS_SERVICE,ROLE_ADMIN",$this->api_token)) 
                        {
                            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
                        }
                        else
                        {
                            $this->load->model('capital_goods_service/mdl_capital_goods_service');
                            $capital_goods_service_master = $this->mdl_capital_goods_service;

                        }
                        break;
                    
                    default:
                        $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
                        break;
                }
            }

            //Check If It's Empty Or What
            if ($branch_query != NULL) 
            {
                foreach ($branch_query as $branch_value) 
                {
                    if ($voucher_id != '') 
                    {
                        $query = $this->db->query("SELECT *,vm.voucher_name as voucher,vm.voucher_id as v_id FROM voucher_master vm INNER JOIN `voucher_group_master` vgm ON vm.voucher_group_id = vgm.voucher_group_id LEFT JOIN `advance_voucher_config_master` avcm ON vm.voucher_id = avcm.voucher_id WHERE vm.voucher_group_id = '".$voucher_group."' AND vm.voucher_id = '".$voucher_id."'");
                    }
                    else
                    {
                        // $query = $this->db->query("SELECT *,vm.voucher_name as voucher,vm.voucher_id as v_id FROM voucher_master vm INNER JOIN `voucher_group_master` vgm ON vm.voucher_group_id = vgm.voucher_group_id LEFT JOIN `advance_voucher_config_master` avcm ON vm.voucher_id = avcm.voucher_id WHERE vm.voucher_group_id = '".$voucher_group."' AND vm.voucher_id = '".$branch_value->voucher_id."'");

                        $query = $this->db->query("SELECT *,vm.voucher_name as voucher,vm.voucher_id as v_id FROM voucher_master vm INNER JOIN `voucher_group_master` vgm ON vm.voucher_group_id = vgm.voucher_group_id LEFT JOIN `advance_voucher_config_master` avcm ON vm.voucher_id = avcm.voucher_id WHERE vm.voucher_group_id = '".$voucher_group."'");
                    }
                    // $v_master = new Model\Model_voucher;
                    // $sale_master = new Model\Model_sale;
                    // $purchase_master = new Model\Model_purchase;
                    // $stock_journal_master = new Model\Model_stock_journal;
                    // $receipt_master = new Model\Model_receipt;
                    // $payment_master = new Model\Model_payment;
                    foreach ($query->result() as $voucher_value) 
                    {
                        $prefix = '';
                        $prefix_date = '';
                        $restart_numbering = '';
                        $restart_numbering_date = '';
                        $suffix = '';
                        $suffix_date = '';
                        $filtered_invoice_no = '';
                        //Validate config if have any
                        if ($voucher_value->advance_voucher_config_id != NULL) 
                        {
                            //check start_from and effective dates
                            $invoice_date = $this->input->post('date',TRUE);
                            if ($invoice_date != '') 
                            {
                                $invoice_date_create = date_create($invoice_date);
                                $date = date_format($invoice_date_create,'Y-m-d');
                            }
                            else
                            {
                                $date = date('Y-m-d');
                            }
                            // $invoice_date = date_create($this->session->userdata('invoice_date'));
                            // $date = date_format($invoice_date,"Y-m-d");
                            $date_create_current = date_create($date);
                            if ($voucher_value->prefix != NULL && $voucher_value->prefix_effective_date != NULL) 
                            {
                                if (!empty($data)) 
                                {
                                    foreach ($data as $data_value) 
                                    {
                                        $prefix = '';
                                        $prefix_date = '';
                                        $suffix = '';
                                        $suffix_date = '';
                                        $filtered_invoice_no = '';
                                        $today = strtotime($date);
                                        $prefix_date_check = strtotime($data_value['prefix_date']);
                                        if ($voucher_value->prefix_effective_date > $date && $prefix_date_check <= $today) 
                                        {
                                            $prefix .= $data_value['prefix'];
                                            $prefix_date .= $data_value['prefix_date'];
                                        }
                                        else if($voucher_value->prefix_effective_date <= $date && $prefix_date_check >= $today)
                                        {
                                            $prefix .= $voucher_value->prefix;
                                            $prefix_date .= $voucher_value->prefix_effective_date;
                                        }
                                        else
                                        {
                                            if ($voucher_value->prefix_effective_date <= $date) 
                                            {
                                                $prefix .= $voucher_value->prefix;
                                                $prefix_date .= $voucher_value->prefix_effective_date;
                                            }
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    if ($voucher_value->prefix_effective_date <= $date) 
                                    {
                                        $prefix .= $voucher_value->prefix;
                                        $prefix_date .= $voucher_value->prefix_effective_date;
                                    }
                                }
                                
                            }
                            else
                            {
                                //null prefix
                                if (!empty($data)) 
                                {
                                    foreach ($data as $data_value) 
                                    {
                                        $prefix .= $data_value['prefix'];
                                        $prefix_date .= $data_value['prefix_date'];
                                    }
                                }

                            }

                            if ($voucher_value->restart_numbering != NULL && $voucher_value->restart_numbering_effective_date != NULL) 
                            {
                                if (!empty($data)) 
                                {
                                    foreach ($data as $data_value) 
                                    {
                                        $restart_numbering = '';
                                        $restart_numbering_date = '';
                                        $today = strtotime($date);
                                        $restart_numbering_date_check = strtotime($data_value['restart_numbering_date']);
                                        if ($voucher_value->restart_numbering_effective_date > $date && $restart_numbering_date_check <= $today) 
                                        {
                                            $restart_numbering .= $data_value['restart_numbering'];
                                            $restart_numbering_date .= $data_value['restart_numbering_date'];
                                        }
                                        else if($voucher_value->restart_numbering_effective_date <= $date && $restart_numbering_date_check >= $today)
                                        {
                                            $restart_numbering .= $voucher_value->restart_numbering;
                                            $restart_numbering_date .= $voucher_value->restart_numbering_effective_date;
                                        }
                                        else
                                        {
                                            if ($voucher_value->restart_numbering_effective_date <= $date) 
                                            {
                                                $date_create = date_create($voucher_value->restart_numbering_effective_date);
                                                $interval = date_diff($date_create, $date_create_current); 
                                                $ts1 = strtotime($voucher_value->restart_numbering_effective_date);
                                                $ts2 = strtotime(date_format($date_create_current,"Y-m-d H:i:s"));

                                                $year1 = date('Y', $ts1);
                                                $year2 = date('Y', $ts2);

                                                $month1 = date('m', $ts1);
                                                $month2 = date('m', $ts2);

                                                $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

                                                $switch_result = $this->_return_restart_num($voucher_value->v_id,$voucher_value->restart_numbering,$voucher_value->restart_numbering_effective_date,$voucher_value->basis,$interval,$diff,$restart_numbering,$restart_numbering_date,$voucher_group,$date_create_current);
                                                $restart_numbering .= $switch_result['restart_numbering'];
                                                $restart_numbering_date .= $switch_result['restart_numbering_effective_date'];
                                                
                                            }
                                            else
                                            {
                                                if ($voucher_group == 1) 
                                                {
                                                    //custom_form
                                                    $count = $custom_form_master->count_voucher_id_with_condition('voucher_id = "'.$voucher_value->v_id.'"'); //Model\Model_sale::find_by_voucher_id($voucher_value->v_id);    
                                                }
                                                
                                                if ($count != NULL && $count > 0) 
                                                {
                                                    $number = $data_value['restart_numbering'] + $count;
                                                    $restart_numbering .= $number;
                                                    $restart_numbering_date .= $data_value['restart_numbering_date'];
                                                }
                                                else
                                                {
                                                    $restart_numbering .= $data_value['restart_numbering'];
                                                    $restart_numbering_date .= $data_value['restart_numbering_date'];
                                                }
                                            }

                                        }
                                    }
                                }
                                else
                                {
                                    if ($date >= $voucher_value->restart_numbering_effective_date) 
                                    {
                                        $restart_numbering_date .= $voucher_value->restart_numbering_effective_date;                            
                                        $date_create = date_create($voucher_value->restart_numbering_effective_date);
                                        $interval = date_diff($date_create, $date_create_current); 
                                        $ts1 = strtotime($voucher_value->restart_numbering_effective_date);
                                        $ts2 = strtotime(date_format($date_create_current,"Y-m-d H:i:s"));

                                        $year1 = date('Y', $ts1);
                                        $year2 = date('Y', $ts2);

                                        $month1 = date('m', $ts1);
                                        $month2 = date('m', $ts2);

                                        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);
                                        // print_r($interval);die();
                                        // printing result in days format
                                        $switch_result = $this->_return_restart_num($voucher_value->v_id,$voucher_value->restart_numbering,$voucher_value->restart_numbering_effective_date,$voucher_value->basis,$interval,$diff,$restart_numbering,$restart_numbering_date,$voucher_group,$date_create_current);
                                        $restart_numbering .= $switch_result['restart_numbering'];
                                        $restart_numbering_date .= $switch_result['restart_numbering_effective_date'];
                                    }
                                    else
                                    {
                                        if ($voucher_group == 1) 
                                        {
                                            //custom_form
                                            $count = $custom_form_master->count_voucher_id_with_condition('voucher_id = "'.$voucher_value->v_id.'"'); //Model\Model_sale::find_by_voucher_id($voucher_value->v_id);    
                                        }
                                        
                                        if ($count != NULL && $count > 0) 
                                        {
                                            $number = $voucher_value->start_from + $count;
                                            $restart_numbering .= $number;
                                        }
                                        else
                                        {
                                            $restart_numbering .= $voucher_value->start_from;
                                        }
                                    }

                                    
                                }
                                
                                
                                
                            }
                            else
                            {
                                // null restart numbering
                                if (!empty($data)) 
                                {
                                    foreach ($data as $data_value) 
                                    {
                                        $restart_numbering = '';
                                        $restart_numbering_date = '';
                                        if ($data_value['restart_numbering'] != '') 
                                        {
                                            $restart_numbering .= $data_value['restart_numbering'];
                                            $restart_numbering_date .= $data_value['restart_numbering_date'];
                                        }
                                        else
                                        {
                                            if ($voucher_group == 1) 
                                            {
                                                //custom_form
                                                $count = $custom_form_master->count_voucher_id_with_condition('voucher_id = "'.$voucher_value->v_id.'"'); //Model\Model_sale::find_by_voucher_id($voucher_value->v_id);    
                                            }
                                            
                                            if ($count != NULL && $count > 0) 
                                            {
                                                $number = $voucher_value->start_from + $count;
                                                $restart_numbering .= $number;
                                            }
                                            else
                                            {
                                                $restart_numbering .= $voucher_value->start_from;
                                            }
                                        }
                                        
                                    }
                                }
                                else
                                {
                                    if ($voucher_group == 1) 
                                    {
                                        //custom_form
                                        $count = $custom_form_master->count_voucher_id_with_condition('voucher_id = "'.$voucher_value->v_id.'"'); //Model\Model_sale::find_by_voucher_id($voucher_value->v_id);    
                                    }
                                    
                                    if ($count != NULL && $count > 0) 
                                    {
                                        $number = $voucher_value->start_from + $count;
                                        $restart_numbering .= $number;
                                    }
                                    else
                                    {
                                        $restart_numbering .= $voucher_value->start_from;
                                    }
                                }
                                
                            }

                            if ($voucher_value->suffix != NULL && $voucher_value->suffix_effective_date != NULL) 
                            {
                                if (!empty($data)) 
                                {
                                    foreach ($data as $data_value) 
                                    {
                                        $suffix = '';
                                        $suffix_date = '';
                                        $today = strtotime($date);
                                        $suffix_date_check = strtotime($data_value['suffix_date']);
                                        if ($voucher_value->suffix_effective_date > $date && $suffix_date_check <= $today) 
                                        {
                                            $suffix .= $data_value['suffix'];
                                            $suffix_date .= $data_value['suffix_date'];
                                        }
                                        else if($voucher_value->suffix_effective_date <= $date && $suffix_date_check >= $today)
                                        {
                                            $suffix .= $voucher_value->suffix;
                                            $suffix_date .= $voucher_value->suffix_effective_date;
                                        }
                                        else
                                        {
                                            if ($voucher_value->suffix_effective_date <= $date) 
                                            {
                                                $suffix .= $voucher_value->suffix;
                                                $suffix_date .= $voucher_value->suffix_effective_date;
                                            }
                                        }
                                        if ($data_value['voucher_id'] == $voucher_value->v_id) 
                                        {
                                            array_pop($data);
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    if ($voucher_value->suffix_effective_date <= $date) 
                                    {
                                        $suffix .= $voucher_value->suffix;
                                        $suffix_date .= $voucher_value->suffix_effective_date;
                                    }
                                }
                            }
                            else
                            {
                                //null suffix
                                if (!empty($data)) 
                                {
                                    foreach ($data as $data_value) 
                                    {
                                        $suffix .= $data_value['suffix'];
                                        $suffix_date .= $data_value['suffix_date'];
                                        if ($data_value['voucher_id'] == $voucher_value->v_id) 
                                        {
                                            array_pop($data);
                                        }
                                    }
                                }
                            }
                            
                        }
                        else
                        {
                            if ($voucher_group == 1) 
                            {
                                //custom_form
                                $count = $custom_form_master->count_voucher_id_with_condition('voucher_id = "'.$voucher_value->v_id.'"'); //Model\Model_sale::find_by_voucher_id($voucher_value->v_id);    
                            }
                            
                            if ($count != NULL && $count > 0) 
                            {
                                $number = $count + 1;
                                $restart_numbering .= $number;
                            }
                            else
                            {
                                $restart_numbering .= 1;
                            }
                        }
                      
                        // $bank_name = "";
                        // $account_number = "";
                        // $branch = "";
                        // $ifs_code = "";
                        // $hsn_summary = 0;
                        // $tax_summary = 0;
                        // $gst_column = 0;
                        // $hsn_column = 0;
                        // $company_name = 0;
                        // $address = 0;
                        // $description = 0;
                        // $declaration = 0;
                        // $print_date = 0;
                        // $sku = 0;
                        // $print_settings_master = Model\Model_print_settings::where('voucher_id',$voucher_value->v_id)->all();
                        // $this->load->model('print_settings/mdl_print_settings');
                        // $condition = array(
                        //     'voucher_id' => $voucher_value->v_id
                        // );
                        // $print_settings_master = $this->mdl_print_settings->get_where_condition($condition)->result();
                        // if (!empty($print_settings_master)) 
                        // {
                        //     if ($print_settings_master[0]->bank_id != NULL) 
                        //     {
                        //         $bank_name = $print_settings_master[0]->bank_master()->bank_name;
                        //         $account_number = $print_settings_master[0]->bank_master()->account_number;
                        //         $branch = $print_settings_master[0]->bank_master()->branch;
                        //         $ifs_code = $print_settings_master[0]->bank_master()->ifs_code;

                        //     }
                        //     $hsn_summary = $print_settings_master[0]->hsn_summary;
                        //     $tax_summary = $print_settings_master[0]->tax_summary;
                        //     $gst_column = $print_settings_master[0]->gst_column;
                        //     $hsn_column = $print_settings_master[0]->hsn_column;
                        //     $company_name = $print_settings_master[0]->company_name;
                        //     $address = $print_settings_master[0]->address;
                        //     $description = $print_settings_master[0]->description;
                        //     $declaration = $print_settings_master[0]->declaration;
                        //     $print_date = $print_settings_master[0]->print_date;
                        //     $sku = $print_settings_master[0]->sku;
                        // }

                        $filtered_invoice_no = $prefix.$restart_numbering.$suffix;
                        $data[] = array(
                            'voucher_id' => $voucher_value->v_id,
                            'voucher_name' => $voucher_value->voucher,
                            'prevent_duplicates' => $voucher_value->prevent_duplicates,
                            'voucher_numbering_method' => $voucher_value->voucher_numbering_method,
                            'filtered_invoice_no' => $filtered_invoice_no,
                            'prefix' => $prefix,
                            'prefix_date' => $prefix_date,
                            'restart_numbering' => $restart_numbering,
                            'restart_numbering_date' => $restart_numbering_date,
                            'suffix' => $suffix,
                            'suffix_date' => $suffix_date,
                            'default_title' => $voucher_value->default_title,
                            'default_jurisdication' => $voucher_value->default_jurisdication,
                            'default_declaration' => $voucher_value->declaration,
                            // 'bank_name' => $bank_name,
                            // 'account_number' => $account_number,
                            // 'branch' => $branch,
                            // 'ifs_code' => $ifs_code,
                            // 'hsn_summary' => $hsn_summary,
                            // 'tax_summary' => $tax_summary,
                            // 'gst_column' => $gst_column,
                            // 'hsn_column' => $hsn_column,
                            // 'company_name' => $company_name,
                            // 'address' => $address,
                            // 'description' => $description,
                            // 'declaration' => $declaration,
                            // 'print_date' => $print_date,
                            // 'sku' => $sku,
                        );
                    }

                }
                
            }
            
            if (empty($data)) 
            {
                $this->response(NULL,MY_Controller::HTTP_NOT_FOUND);
            }

            $this->response($data,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
   }
   public function _return_restart_num($voucher_id,$restart,$restart_numbering_effective_date,$basis,$interval,$diff,$restart_numbering,$restart_numbering_date,$voucher_group,$date_create_current)
   {
     $is_current_yearly_basis = FALSE;
     $is_current_monthly_basis = FALSE;
     $is_current_weekly_basis = FALSE;
     $is_current_daily_basis = FALSE;
     // $v_master = new Model\Model_voucher;
     // $sale_master = new Model\Model_sale;
     // $purchase_master = new Model\Model_purchase;
     // $stock_journal_master = new Model\Model_stock_journal;
     // $receipt_master = new Model\Model_receipt;
     // $payment_master = new Model\Model_payment;
     // $custom_form_master = $this->mdl_custom_form;
     // $bom_master = $this->mdl_bom;
     switch (intval($voucher_group)) {
        case 1:
            //CUSTOM_FORM
            $this->load->model('custom_form/mdl_custom_form');
            $custom_form_master = $this->mdl_custom_form;
            break;
        
        default:
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
            break;
    }

     switch ($basis) {
        case 'Yearly':
            //invoice_date wise addition in yearly basis
            for ($i=0; $i < $interval->y; $i++) 
            { 
                if ($i == $interval->y - 1 && $interval->m == 0 && $interval->d == 0) 
                {
                    $is_current_yearly_basis = TRUE;
                }
                else
                {
                    $is_current_yearly_basis = FALSE;
                }
            }
            switch ($is_current_yearly_basis) 
            {
                case TRUE:
                    $dateString = $restart_numbering_effective_date;
                    $t = strtotime($dateString);
                    $check_date = date('Y-m-d',strtotime('+'.$interval->y.' years', $t)); //effective_date_year + interval->y;
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('date(custom_form_date) >= date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"'); //Model\Model_sale::where('date(invoice_date) >= date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"')->all();   
                    }
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + intval($restart);
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    else
                    {
                        $restart_numbering .= $restart;
                        $restart_numbering_date .= $restart_numbering_effective_date;
                    }
                    break;

                case FALSE:
                    $dateString = $restart_numbering_effective_date;
                    $t = strtotime($dateString);
                    $check_date = date('Y-m-d',strtotime('+'.$interval->y.' years', $t)); //effective_date_year + interval->y;
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('date(custom_form_date) >= date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"'); //Model\Model_sale::where('date(invoice_date) >= date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"')->all();   
                    }
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + intval($restart);
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    else
                    {
                        $restart_numbering .= $restart;
                        $restart_numbering_date .= $restart_numbering_effective_date;
                    }
                    break;
                
                default:
                    break;
            }
            break;
        case 'Monthly':
            //invoice_date wise addition in monthly basis
            for ($i=0; $i < $diff; $i++) 
            { 
                if ($i == $diff - 1) 
                {
                    $is_current_monthly_basis = TRUE;
                }
                else
                {
                    $is_current_monthly_basis = FALSE;
                }
            }
            switch ($is_current_monthly_basis) 
            {
                case TRUE:
                    $dateString = $restart_numbering_effective_date;
                    $t = strtotime($dateString);
                    $check_date = date('Y-m-d',strtotime('+'.$diff.' months', $t)); //effective_date_year + interval->y;
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('month(custom_form_date) = month("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('month(invoice_date) = month("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"')->all();   
                    }
                    
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + intval($restart);
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    else
                    {
                        if ($voucher_group == 1) 
                        {
                            //custom_form
                            $count = $custom_form_master->count_voucher_id_with_condition('month(custom_form_date) = month("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('month(invoice_date) = month("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"')->all();   
                        }
                        
                        
                        if ($count != NULL && $count > 0) 
                        {
                            $number = $count + intval($restart);
                            $restart_numbering .= $number;
                            $restart_numbering_date .= $restart_numbering_effective_date;
                        }
                        else
                        {
                            $restart_numbering .= $restart;
                            $restart_numbering_date .= $restart_numbering_effective_date;
                        }
                    }
                    break;

                case FALSE:
                    $dateString = $restart_numbering_effective_date;
                    $t = strtotime($dateString);
                    $check_date = date('Y-m-d',strtotime('+'.$diff.' months', $t)); //effective_date_year + interval->y;
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('month(custom_form_date) = month("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('month(invoice_date) = month("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"')->all();   
                    }
                    
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + 1;
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    else
                    {
                        $restart_numbering .= $restart;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    break;
                
                default:
                    break;
            }
            break;
        case 'Weekly':
            //invoice_date wise addition in weekly basis
            $weeks = is_float($interval->days/7) ? 0 : floor($interval->days/7);
            $date_create_current_format = date_format($date_create_current,"Y-m-d H:i:s");
            for ($i=0; $i < $weeks; $i++) 
            { 
                if ($i == $weeks - 1) 
                {
                    $is_current_weekly_basis = TRUE;
                }
                else
                {
                    $is_current_weekly_basis = FALSE;
                }
            }
            switch ($is_current_weekly_basis) {
                case TRUE:
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('YEARWEEK(custom_form_date, 1) = YEARWEEK("'.$date_create_current_format.'", 1) AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('YEARWEEK(invoice_date, 1) = YEARWEEK("'.$date_create_current_format.'", 1) AND voucher_id = "'.$voucher_id.'"')->all();

                    }
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + intval($restart);
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    else
                    {
                        $restart_numbering .= $restart;
                        $restart_numbering_date .= $restart_numbering_effective_date;
                    }
                    break;

                case FALSE:
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('YEARWEEK(custom_form_date, 1) = YEARWEEK("'.$date_create_current_format.'", 1) AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('YEARWEEK(invoice_date, 1) = YEARWEEK("'.$date_create_current_format.'", 1) AND voucher_id = "'.$voucher_id.'"')->all();
                    }
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + intval($restart);
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    else
                    {
                        $restart_numbering .= $restart;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    break;
                
                default:
                    break;
            }
            break;
        case 'Daily':
            //invoice_date wise addition in daily basis
            for ($i=0; $i < $interval->d; $i++) 
            { 
                if ($i == $interval->d - 1) 
                {
                    $is_current_daily_basis = TRUE;
                }
                else
                {
                    $is_current_daily_basis = FALSE;
                }
            }
            switch ($is_current_daily_basis) {
                case TRUE:
                    $dateString = $restart_numbering_effective_date;
                    $t = strtotime($dateString);
                    $check_date = date('Y-m-d',strtotime('+'.$interval->d.' days', $t)); //effective_date_year + interval->y;
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('date(custom_form_date) = date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('date(invoice_date) = date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"')->all();
                    }
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + intval($restart);
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;
                    }
                    else
                    {
                        $restart_numbering .= $restart;
                        $restart_numbering_date .= $restart_numbering_effective_date;
                    }
                    break;
                case FALSE:
                    $dateString = $restart_numbering_effective_date;
                    $t = strtotime($dateString);
                    $check_date = date('Y-m-d',strtotime('+'.$interval->d.' days', $t)); //effective_date_year + interval->y;
                    if ($voucher_group == 1) 
                    {
                        //custom_form
                        $count = $custom_form_master->count_voucher_id_with_condition('date(custom_form_date) = date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('date(invoice_date) = date("'.$check_date.'") AND voucher_id = "'.$voucher_id.'"')->all();
                    }
                    
                    
                    if ($count != NULL && $count > 0) 
                    {
                        $number = $count + intval($restart);
                        $restart_numbering .= $number;
                        $restart_numbering_date .= $restart_numbering_effective_date;
                    }
                    else
                    {
                        $restart_numbering .= $restart;
                        $restart_numbering_date .= $restart_numbering_effective_date;

                    }
                    break;
                
                default:
                    break;
            }
            break;
        case 'Never':
            if ($voucher_group == 1) 
            {
                //custom_form
                $count = $custom_form_master->count_voucher_id_with_condition('date(custom_form_date) >= "'.$restart_numbering_effective_date.'" AND voucher_id = "'.$voucher_id.'"');//Model\Model_sale::where('date(invoice_date) >= "'.$restart_numbering_effective_date.'" AND voucher_id = "'.$voucher_id.'"')->all();
            }
            
            
            if ($count != NULL && $count > 0) 
            {
                $number = intval($restart_numbering) + $count;
                $restart_numbering .= $number;
                $restart_numbering_date .= $restart_numbering_effective_date;
            }
            else
            {
                $restart_numbering .= $restart;
                $restart_numbering_date .= $restart_numbering_effective_date;
            }
            break;
        default:
            break;
    }
    return array('restart_numbering' => $restart_numbering,'restart_numbering_effective_date' => $restart_numbering_effective_date);

   }

   public function get_all_voucher_details_post($voucher_id=NULL)
   {
        if(authorize("CENTRAL",$this->api_token) || authorize("STATE",$this->api_token) || authorize("UNIT",$this->api_token)) 
        {
            $data = array();

            $user_master = $this->db->where('id = '.$this->db->escape($this->api_token->user_id).'')->get('user_master')->result();
            if (empty($user_master)) 
            {
                $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
            }
            if ($voucher_id != NULL) 
            {
                $condition = array(
                    'voucher_id' => $voucher_id,
                );
                $voucher_master = $this->mdl_voucher->get_where_condition($condition)->result();
                if (empty($voucher_master)) 
                {
                    $this->response(NULL,MY_Controller::HTTP_METHOD_NOT_ALLOWED);
                }

                if (!empty($voucher_master)) 
                {

                    foreach ($voucher_master as $voucher_value) 
                    {

                        $data[] = array(
                            'voucher_id' => $voucher_value->voucher_id,
                            'voucher_name' => $voucher_value->voucher_name,
                        );
                    }
                }
            }
                
            if (empty($data)) 
            {
                $this->response(NULL,MY_Controller::HTTP_NOT_FOUND);
            }
            $this->response($data,MY_Controller::HTTP_OK);
        }
        else
        {
            $this->response(NULL,MY_Controller::HTTP_FORBIDDEN);
        }
    }


    function _get($order_by)
    {
        $this->load->model('mdl_voucher');
        $query = $this->mdl_voucher->get($order_by);
        return $query;
    }

    function _get_with_limit_where($limit, $offset, $order_by, $dir, $condition) 
    {
        if ((!is_numeric($limit)) || (!is_numeric($offset))) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_voucher');
        $query = $this->mdl_voucher->get_with_limit_where($limit, $offset, $order_by, $dir, $condition);
        return $query;
    }

    function _get_with_limit($limit, $offset, $order_by, $dir) 
    {
        if ((!is_numeric($limit)) || (!is_numeric($offset))) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_voucher');
        $query = $this->mdl_voucher->get_with_limit($limit, $offset, $order_by, $dir);
        return $query;
    }

    function _get_where($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_voucher');
        $query = $this->mdl_voucher->get_where($id);
        return $query;
    }

    function _get_where_custom($col, $value) 
    {
        $this->load->model('mdl_voucher');
        $query = $this->mdl_voucher->get_where_custom($col, $value);
        return $query;
    }

    function _insert($data)
    {
        $this->load->model('mdl_voucher');
        return $this->mdl_voucher->_insert($data);
    }


    function _update($id, $data)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_voucher');
        return $this->mdl_voucher->_update($id, $data);
    }

    function _delete($id)
    {
        if (!is_numeric($id)) {
            die('Non-numeric variable!');
        }

        $this->load->model('mdl_voucher');
        return $this->mdl_voucher->_delete($id);
    }

    function _count_where($column, $value) 
    {
        $this->load->model('mdl_voucher');
        $count = $this->mdl_voucher->count_where($column, $value);
        return $count;
    }

    function _count_all() 
    {
        $this->load->model('mdl_voucher');
        $count = $this->mdl_voucher->count_all();
        return $count;
    }

    function _get_max() 
    {
        $this->load->model('mdl_voucher');
        $max_id = $this->mdl_voucher->get_max();
        return $max_id;
    }

    function _custom_query($mysql_query) 
    {
        $this->load->model('mdl_voucher');
        $query = $this->mdl_voucher->_custom_query($mysql_query);
        return $query;
    }
    

}