<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('validate_date'))
{
//custom_validate_date
function validate_date($date, $format='Y-m-d'){
    $d = DateTime::createFromFormat($format, date($format, strtotime($date)));
    return $d && $d->format($format) === date($format, strtotime($date));
}

}