<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if( ! function_exists('authorize') )
{
	function authorize( $str, $data )
	{
		
        $CI =& get_instance();
        // get permissions from token
        

        /*$u_alloc = $CI->db->where('id = '.$CI->db->escape($data->user_id).'')->get('user_master')->result();
        if(empty($u_alloc))
        {
            return false;
        }*/

        // $b_alloc_check = Model\Model_branch_user_alloc::find_by_user_id($data->user_id);
        $loc_check = $CI->db->where('user_id = '.$CI->db->escape($data->user_id).'')->get('location_master')->result();
        if(empty($loc_check))
        {
            return false;
        }
        $level_check = $CI->db->where('id = '.$CI->db->escape($loc_check[0]->level_id).'')->get('level_master')->result_array();
        
        if(empty($level_check))
        {
            return false;
        }
        $levels = array_map('strtolower', array_column($level_check,'level_name'));
		//print_r(strtolower($str));exit;
        if(!in_array(strtolower($str),$levels))
        {
            return false;
        }
        else
        {
            return true;
        }
        
	}
}


/*
*/
