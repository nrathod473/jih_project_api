<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('call_api'))
{
    function call_api($method, $url, $data){
    $curl = curl_init();
    switch ($method){
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                $url = sprintf("%s?%s", $url, http_build_query($data));
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                $url = sprintf("%s?%s", $url, http_build_query($data));
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
            break;
    }

    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Accept: application/json'
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    // EXECUTE:
    $result = curl_exec($curl);

    if(!$result) {
        die("Connection Failure");
    }

    curl_close($curl);

    return $result;
}

}

if ( ! function_exists('callapi'))
{
//custom_call_api
function callapi($method, $url, $data,$token){
    $curl = curl_init();
    // $url = _mysiteURL().$url;
    $url = $url;
    switch ($method){
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if (is_array($data))
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if (is_array($data))
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }

    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'X-Auth-Token:'.$token,
    ));
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    // EXECUTE:
    $result = curl_exec($curl);
    
    if(!$result) {
        // die("Connection Failure");
        return FALSE;
    }

    curl_close($curl);

    return $result;
}
function callapi_multi_post($datas){
    // array of curl handles
    $multiCurl = array();
    // data to be returned
    $result = array();
    // multi handle
    $mh = curl_multi_init();
    foreach ($datas as $i => $row) {
      // URL from which data will be fetched
      $fetchURL = $row['url'];
      $multiCurl[$i] = curl_init();
      curl_setopt($multiCurl[$i], CURLOPT_POST, 1);
      curl_setopt($multiCurl[$i], CURLOPT_URL,$fetchURL);
      curl_setopt($multiCurl[$i], CURLOPT_POST, 1);
      curl_setopt($multiCurl[$i], CURLOPT_POSTFIELDS, $row['data']);
      curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
      curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER,1);
      curl_multi_add_handle($mh, $multiCurl[$i]);
    }
    $index=null;
    do {
      curl_multi_exec($mh,$index);
    } while($index > 0);
    // get content and remove handles
    foreach($multiCurl as $k => $ch) {
      if (curl_multi_getcontent($ch)) 
      {
        $result[$k] = curl_multi_getcontent($ch);  
      }
      curl_multi_remove_handle($mh, $ch);
    }

    if(!$result) {
        // die("Connection Failure");
        return FALSE;
    }

    // close
    curl_multi_close($mh);

    return $result;
}
function _mysiteURL() 
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol . $domainName;
}

}