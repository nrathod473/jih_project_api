<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('attachment'))
{
//attachment

function _get_table() {
    $table = "attachment_master";
    return $table;
}

function add_attachment($data)
{
    $CI =& get_instance();
    $table = _get_table();
    if ($CI->db->insert($table,$data)) 
    {
        return $CI->db->insert_id();
    }
    else
    {
        return false;
    }
}

function update_attachment($id,$data)
{
    $CI =& get_instance();
    $table = _get_table();
    $CI->db->query('SELECT id FROM '.$table.' WHERE id = "'.$id.'" FOR UPDATE');
    if ($CI->db->where('id',$id)->update($table,$data)) 
    {
        return $id;
    }
    else
    {
        return false;
    }
}

function remove_attachment($id){
    $CI =& get_instance();
    $table = get_table();
    $CI->db->where('id', $id);
    if ($CI->db->delete($table)) 
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

function get_where_custom_attachment($col, $value) {
    $CI =& get_instance();
    $table = _get_table();
    $CI->db->where($col, $value);
    $query=$CI->db->get($table);
    return $query;
}

}