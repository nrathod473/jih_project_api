<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/


$hook['cron_frequent'][] = array(
'class' => 'Tally_fetch',
'function' => 'fetch_part_rate_uom',
'filename' => 'Tally_fetch.php',
'filepath' => 'modules/tally_fetch/controllers',
'params' => array(NULL,'1xhV7dDR5Z')
);


// $hook['cron_hourly'][] = array(
// 'class' => 'Welcome',
// 'function' => 'index',
// 'filename' => 'Welcome.php',
// 'filepath' => 'controllers',
// 'params' => array('beer', 'wine', 'snacks')
// );

// $hook['cron_daily'][] = array(
// 'class' => 'Welcome',
// 'function' => 'index',
// 'filename' => 'Welcome.php',
// 'filepath' => 'controllers',
// 'params' => array('beer', 'wine', 'snacks')
// );

